
# ZIF

A Z-machine interpreter for interactive fiction.

### Getting started

You need [Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) to be able to build the program.
No binaries are currently available. 

Build instructions:

`cargo build`

To run the program, pass the path to a Z-code program as an argument to the program. 