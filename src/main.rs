extern crate getopts;
extern crate rand;

use common::machine::create_machine;
use getopts::Options;
use getopts::ParsingStyle;
use std::env;
use std::string::String;
use v5::disassemble::print_disassembly;
use common::address::AbsoluteAddress;

mod v5;
mod common;

fn print_usage(program: &String, options: Options) {
    let brief = format!(
        "Usage: {} [OPTIONS] FILE

This program is an interpreter implementing the Z-Machine virtual machine.",
        program);

    print!("{}", options.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = &args[0];

    let mut options = Options::new();
    options.parsing_style(ParsingStyle::StopAtFirstFree);
    options.optopt("t", "transcript", "the file to write the transcript to", "FILE");
    options.optflag("h", "help", "print the usage for this program");
    options.optflag("v", "verbose", "print verbose interpreter output");
    options.optflag("p", "print-memory", "print memory dump and quit");
    options.optflag("d", "print-disassembly", "print disassembly and quit");
    options.optflag("i", "interactive", "run in interactive mode, implies verbose output");
    options.optopt("b", "breakpoints", "BREAKPOINTS", "set the list of memory addresses as break points");
    let matches = match options.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()) // Options are not valid
    };

    // Check for help option
    if matches.opt_present("h") {
        print_usage(&program, options);
        return;
    }

    // Check that a single path has been provided to be interpreted
    if matches.free.len() != 1 {
        print_usage(&program, options);
        return;
    }

    let path = &matches.free[0];
    if matches.opt_present("d") {
        match print_disassembly(path) {
            Ok(_) => {},
            Err(e) => eprintln!("Error {}", e)
        }
    } else {
        let breakpoint_strings: Vec<String> = matches.opt_str("b").map(|p| p.split(',').map(|bp| bp.to_string()).collect()).unwrap_or(Vec::new());
        let mut breakpoints: Vec<AbsoluteAddress> = Vec::new();
        for bp in breakpoint_strings {
            let addr: usize = match usize::from_str_radix(&bp, 16) {
                Ok(addr) => addr,
                Err(e) => {
                    eprintln!("Error {}", e);
                    return;
                }
            };
            breakpoints.push(AbsoluteAddress(addr));
        }

        match create_machine(path, matches.opt_present("v") || matches.opt_present("i"), matches.opt_str("t"), matches.opt_present("i"), breakpoints) {
            Ok(m) => {
                if matches.opt_present("p") {
                    m.print_memory().expect("Failed to write the memory");
                } else {
                    m.run();
                }
            },
            Err(e) => eprintln!("Error {}", e)
        };
    }
}
