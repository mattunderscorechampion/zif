
#[derive(Debug, PartialEq, Eq)]
pub enum OperandType {
    LargeConstant,
    SmallConstant,
    Variable,
    Omitted
}

#[derive(Debug, PartialEq, Eq)]
pub enum Operand {
    LargeConstant(u16),
    SmallConstant(u8),
    Variable(u8)
}

pub fn decode_1bit_operand_type(operand_type: u8) -> OperandType {
    match operand_type {
        0 => OperandType::SmallConstant,
        1 => OperandType::Variable,
        _ => panic!("1-bit value > 1")
    }
}

fn decode_2bit_operand_type(operand_type: u8) -> OperandType {
    match operand_type {
        0 => OperandType::LargeConstant,
        1 => OperandType::SmallConstant,
        2 => OperandType::Variable,
        3 => OperandType::Omitted,
        _ => panic!("2-bit value > 3")
    }
}

pub fn operand_type(byte: u8, operand_number: u8) -> OperandType {
    assert!(operand_number < 4, "Only 1-4 operands can be specified in a byte");
    let operand_offset = (3 - operand_number) * 2;
    decode_2bit_operand_type((byte >> operand_offset) & 0b11)
}

pub fn operand_types(byte: u8) -> Vec<OperandType> {
    let mut operands = Vec::new();
    for operand_number in 0..4 {
        let operand_type = operand_type(byte, operand_number);
        match operand_type {
            OperandType::Omitted => {
                return operands
            },
            t => operands.push(t)
        }
    }
    operands
}

mod operand_type_tests {
    use super::operand_type;
    use super::operand_types;
    use super::OperandType;

    #[test]
    fn operand_type_large_constant() {
        assert_eq!(OperandType::LargeConstant, operand_type(0b00000000, 0));
        assert_eq!(OperandType::LargeConstant, operand_type(0b00000000, 1));
        assert_eq!(OperandType::LargeConstant, operand_type(0b00000000, 2));
        assert_eq!(OperandType::LargeConstant, operand_type(0b00000000, 3));
    }

    #[test]
    fn operand_type_small_constant() {
        assert_eq!(OperandType::SmallConstant, operand_type(0b01010101, 0));
        assert_eq!(OperandType::SmallConstant, operand_type(0b01010101, 1));
        assert_eq!(OperandType::SmallConstant, operand_type(0b01010101, 2));
        assert_eq!(OperandType::SmallConstant, operand_type(0b01010101, 3));
    }

    #[test]
    fn operand_type_variable() {
        assert_eq!(OperandType::Variable, operand_type(0b10101010, 0));
        assert_eq!(OperandType::Variable, operand_type(0b10101010, 1));
        assert_eq!(OperandType::Variable, operand_type(0b10101010, 2));
        assert_eq!(OperandType::Variable, operand_type(0b10101010, 3));
    }

    #[test]
    fn operand_type_omitted() {
        assert_eq!(OperandType::Omitted, operand_type(0b11111111, 0));
        assert_eq!(OperandType::Omitted, operand_type(0b11111111, 1));
        assert_eq!(OperandType::Omitted, operand_type(0b11111111, 2));
        assert_eq!(OperandType::Omitted, operand_type(0b11111111, 3));
    }

    #[test]
    fn operand_position() {
        assert_eq!(OperandType::SmallConstant, operand_type(0b01000000, 0));
        assert_eq!(OperandType::SmallConstant, operand_type(0b00010000, 1));
        assert_eq!(OperandType::SmallConstant, operand_type(0b00000100, 2));
        assert_eq!(OperandType::SmallConstant, operand_type(0b00000001, 3));
    }

    #[test]
    fn all_operand_types_1_operand() {
        assert_eq!(
            vec!{
                OperandType::LargeConstant
            },
            operand_types(0b00111111));
    }

    #[test]
    fn all_operand_types_3_operands() {
        assert_eq!(
            vec!{
                OperandType::LargeConstant,
                OperandType::SmallConstant,
                OperandType::Variable
            },
            operand_types(0b00011011));
    }
}

