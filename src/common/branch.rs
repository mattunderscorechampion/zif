use common::address::AbsoluteAddress;
use common::memory::Memory;
use std::mem::transmute;

#[derive(Debug, PartialEq, Eq)]
pub enum BranchArgument {
    Jump {
        reverse_logic: bool,
        target_address: AbsoluteAddress,
        resume_address: AbsoluteAddress
    },
    ReturnFalse {
        reverse_logic: bool,
        resume_address: AbsoluteAddress
    },
    ReturnTrue {
        reverse_logic: bool,
        resume_address: AbsoluteAddress
    }
}
impl BranchArgument {
    pub fn decode(memory: &Memory, address: AbsoluteAddress) -> (BranchArgument, usize) {
        let branch_argument = memory.at(address);
        let reverse_logic = branch_argument & 0b10000000 == 0;
        let single_byte = branch_argument & 0b01000000 != 0;
        let offset = if single_byte {
            (branch_argument & 0b00111111) as i16
        } else {
            let top = if branch_argument & 0b00100000 == 0 {
                branch_argument & 0b00011111
            } else {
                0b11100000 | branch_argument & 0b00011111
            };
            let bottom = memory.at(address + 1);
            let bytes : [u8; 2] = [bottom, top];
            unsafe {
                transmute::<[u8; 2], i16>(bytes)
            }
        };

        if offset == 0 {
            (BranchArgument::ReturnFalse {
                reverse_logic,
                resume_address: address + 1
            }, 1)
        } else if offset == 1 {
            (BranchArgument::ReturnTrue {
                reverse_logic,
                resume_address: address + 1
            }, 1)
        } else {
            let target_address = if single_byte { address + offset - 1 } else { address + offset };
            let resume_address = address + if single_byte { 1 } else { 2 };

            (BranchArgument::Jump {
                reverse_logic,
                target_address,
                resume_address
            }, if single_byte { 1 } else { 2 })
        }
    }
}
