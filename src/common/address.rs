use std::fmt;
use std::fmt::Formatter;
use std::fmt::UpperHex;
use std::ops::Add;
use std::ops::Sub;
use std::ops::AddAssign;
use std::cmp::Ordering;

pub trait ToAbsoluteAddress {
    fn to_address(&self) -> AbsoluteAddress;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct AbsoluteAddress(pub usize);
impl UpperHex for AbsoluteAddress {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let width = f.width().unwrap_or(0);
        let format = format!("{:.*X}", width, self.0);
        f.pad_integral(true, "0", &format)
    }
}
impl ToAbsoluteAddress for AbsoluteAddress {
    fn to_address(&self) -> AbsoluteAddress {
        AbsoluteAddress(self.0)
    }
}
impl Add<usize> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn add(self, rhs: usize) -> <Self as Add<usize>>::Output {
        AbsoluteAddress(self.0 + rhs)
    }
}
impl Sub<usize> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn sub(self, rhs: usize) -> <Self as Sub<usize>>::Output {
        AbsoluteAddress(self.0 - rhs)
    }
}
impl AddAssign<usize> for AbsoluteAddress {
    fn add_assign(&mut self, rhs: usize) {
        *self = AbsoluteAddress(self.0 + rhs);
    }
}
impl PartialEq<usize> for AbsoluteAddress {
    fn eq(&self, other: &usize) -> bool {
        self.0 == *other
    }
}
impl PartialOrd<usize> for AbsoluteAddress {
    fn partial_cmp(&self, other: &usize) -> Option<Ordering> {
        Some(self.0.cmp(other))
    }
}
impl Add<i16> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn add(self, rhs: i16) -> <Self as Add<usize>>::Output {
        if rhs < 0 {
            AbsoluteAddress(self.0 - ((-rhs) as usize))
        }
        else {
            AbsoluteAddress(self.0 + (rhs as usize))
        }
    }
}
impl Sub<i16> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn sub(self, rhs: i16) -> <Self as Sub<usize>>::Output {
        if rhs < 0 {
            AbsoluteAddress(self.0 + ((-rhs) as usize))
        }
        else {
            AbsoluteAddress(self.0 - (rhs as usize))
        }
    }
}
impl AddAssign<i16> for AbsoluteAddress {
    fn add_assign(&mut self, rhs: i16) {
        *self = if rhs < 0 {
            AbsoluteAddress(self.0 - ((-rhs) as usize))
        }
        else {
            AbsoluteAddress(self.0 + (rhs as usize))
        };
    }
}
impl Add<u16> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn add(self, rhs: u16) -> <Self as Add<usize>>::Output {
        AbsoluteAddress(self.0 + (rhs as usize))
    }
}
impl Sub<u16> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn sub(self, rhs: u16) -> <Self as Sub<usize>>::Output {
        AbsoluteAddress(self.0 - (rhs as usize))
    }
}
impl AddAssign<u16> for AbsoluteAddress {
    fn add_assign(&mut self, rhs: u16) {
        *self = AbsoluteAddress(self.0 + (rhs as usize))
    }
}
impl Add<i32> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn add(self, rhs: i32) -> <Self as Add<usize>>::Output {
        if rhs < 0 {
            AbsoluteAddress(self.0 - ((-rhs) as usize))
        }
            else {
                AbsoluteAddress(self.0 + (rhs as usize))
            }
    }
}
impl Sub<i32> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn sub(self, rhs: i32) -> <Self as Sub<usize>>::Output {
        if rhs < 0 {
            AbsoluteAddress(self.0 + ((-rhs) as usize))
        }
            else {
                AbsoluteAddress(self.0 - (rhs as usize))
            }
    }
}
impl AddAssign<i32> for AbsoluteAddress {
    fn add_assign(&mut self, rhs: i32) {
        *self = if rhs < 0 {
            AbsoluteAddress(self.0 - ((-rhs) as usize))
        }
            else {
                AbsoluteAddress(self.0 + (rhs as usize))
            };
    }
}
impl Add<u8> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn add(self, rhs: u8) -> <Self as Add<usize>>::Output {
        AbsoluteAddress(self.0 + rhs as usize)
    }
}
impl Sub<u8> for AbsoluteAddress {
    type Output = AbsoluteAddress;

    fn sub(self, rhs: u8) -> <Self as Sub<usize>>::Output {
        AbsoluteAddress(self.0 - rhs as usize)
    }
}
impl AddAssign<u8> for AbsoluteAddress {
    fn add_assign(&mut self, rhs: u8) {
        *self = AbsoluteAddress(self.0 + rhs as usize);
    }
}


#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct ByteAddress(pub u16);
impl UpperHex for ByteAddress {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let width = f.width().unwrap_or(0);
        let format = format!("{:.*X}", width, self.0);
        f.pad_integral(true, "0", &format)
    }
}
impl ToAbsoluteAddress for ByteAddress {
    fn to_address(&self) -> AbsoluteAddress {
        AbsoluteAddress(self.0 as usize)
    }
}
impl Into<u16> for ByteAddress {
    fn into(self) -> u16 {
        self.0
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct WordAddress(pub u16);
impl UpperHex for WordAddress {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let width = f.width().unwrap_or(0);
        let format = format!("{:.*X}", width, self.to_address());
        f.pad_integral(true, "0", &format)
    }
}
impl ToAbsoluteAddress for WordAddress {
    fn to_address(&self) -> AbsoluteAddress {
        AbsoluteAddress(self.0 as usize * 2)
    }
}

mod address_tests {
    use common::address::AbsoluteAddress;

    #[test]
    fn add_usize() {
        let address = AbsoluteAddress(0x400);
        let offset: usize = 0x40;
        let offset_address = address + offset;

        assert_eq!(offset_address, AbsoluteAddress(0x440));
    }

    #[test]
    fn subtract_usize() {
        let address = AbsoluteAddress(0x400);
        let offset: usize = 0x40;
        let offset_address = address - offset;

        assert_eq!(offset_address, AbsoluteAddress(0x3C0));
    }

    #[test]
    fn add_i32() {
        let address = AbsoluteAddress(0x400);
        let offset: i32 = 0x40;
        let offset_address = address + offset;

        assert_eq!(offset_address, AbsoluteAddress(0x440));
    }

    #[test]
    fn subtract_i32() {
        let address = AbsoluteAddress(0x400);
        let offset: i32 = 0x40;
        let offset_address = address - offset;

        assert_eq!(offset_address, AbsoluteAddress(0x3C0));
    }

    #[test]
    fn add_i16() {
        let address = AbsoluteAddress(0x400);
        let offset: i16 = 0x40;
        let offset_address = address + offset;

        assert_eq!(offset_address, AbsoluteAddress(0x440));
    }

    #[test]
    fn subtract_i16() {
        let address = AbsoluteAddress(0x400);
        let offset: i16 = 0x40;
        let offset_address = address - offset;

        assert_eq!(offset_address, AbsoluteAddress(0x3C0));
    }

    #[test]
    fn add_u16() {
        let address = AbsoluteAddress(0x400);
        let offset: u16 = 0x40;
        let offset_address = address + offset;

        assert_eq!(offset_address, AbsoluteAddress(0x440));
    }

    #[test]
    fn subtract_u16() {
        let address = AbsoluteAddress(0x400);
        let offset: u16 = 0x40;
        let offset_address = address - offset;

        assert_eq!(offset_address, AbsoluteAddress(0x3C0));
    }
}
