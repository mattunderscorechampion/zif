use std::fs::File;
use std::io::Read;
use std::io::Result;
use std::io::stdin;

pub enum InputStream {
    Stdin,
    File {
        file: File
    }
}
impl InputStream {
    pub fn stdin() -> InputStream {
        InputStream::Stdin
    }

    pub fn file(file: File) -> InputStream {
        InputStream::File{ file }
    }
}

impl Read for InputStream {

    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        match self {
            InputStream::Stdin => stdin().read(buf),
            InputStream::File { file } => file.read(buf)
        }
    }
}
