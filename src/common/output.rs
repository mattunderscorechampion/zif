use std::fs::File;
use std::io::Write;
use std::io::Error;
use std::io::sink;
use std::io::stdout;
use std::io::stderr;

pub enum OutputStream {
    Sink,
    Stdout,
    Stderr,
    File {
        file: File
    }
}
impl OutputStream {
    fn sink() -> OutputStream {
        OutputStream::Sink
    }

    fn stdout() -> OutputStream {
        OutputStream::Stdout
    }

    fn file(file: File) -> OutputStream {
        OutputStream::File{ file }
    }
}

impl Write for OutputStream {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        match self {
            OutputStream::Sink => sink().write(buf),
            OutputStream::Stdout => stdout().write(buf),
            OutputStream::Stderr => stderr().write(buf),
            OutputStream::File { file } => file.write(buf)
        }
    }

    fn flush(&mut self) -> Result<(), Error> {
        match self {
            OutputStream::Sink => sink().flush(),
            OutputStream::Stdout => stdout().flush(),
            OutputStream::Stderr => stderr().flush(),
            OutputStream::File { file } => file.flush()
        }
    }
}
