use common::address::AbsoluteAddress;
use common::memory::Memory;
use common::operand::decode_1bit_operand_type;
use common::operand::Operand;
use common::operand::operand_type;
use common::operand::operand_types;
use common::operand::OperandType;

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Form {
    Long,
    Short,
    Extended,
    Variable
}

#[derive(Debug, PartialEq, Eq)]
pub struct Instruction {
    pub form: Form,
    pub opcode: u8,
    pub operands: Vec<Operand>,
    pub size: usize
}

fn decode_operands(memory: &Memory, operand_types: Vec<OperandType>, mut operand_position: AbsoluteAddress) -> (Vec<Operand>, usize) {
    let mut operands = Vec::new();
    let mut bytes_read = 0;
    for operand_type in operand_types {
        match operand_type {
            OperandType::LargeConstant => {
                operands.push(Operand::LargeConstant(memory.u16_at(operand_position)));
                operand_position += 2;
                bytes_read += 2;
            },
            OperandType::SmallConstant => {
                operands.push(Operand::SmallConstant(memory.at(operand_position)));
                operand_position += 1;
                bytes_read += 1;
            },
            OperandType::Variable => {
                operands.push(Operand::Variable(memory.at(operand_position)));
                operand_position += 1;
                bytes_read += 1;
            },
            OperandType::Omitted => break
        }
    }

    (operands, bytes_read)
}

pub fn decode_short_form_instruction(memory: &Memory, position: AbsoluteAddress) -> Instruction {
    let opcode = memory.at(position);
    let operand_type = operand_type(opcode, 1);
    let operands = decode_operands(memory, vec! {operand_type}, position + 1);

    Instruction {
        form: Form::Short,
        opcode,
        operands: operands.0,
        size: 1 + operands.1
    }
}

pub fn decode_long_form_instruction(memory: &Memory, position: AbsoluteAddress) -> Instruction {
    let byte = memory.at(position);
    let operand_0_type = decode_1bit_operand_type((byte & 0b01000000) >> 6);
    let operand_1_type = decode_1bit_operand_type((byte & 0b00100000) >> 5);
    let opcode = byte & 0b00011111;
    let operands = decode_operands(memory, vec! {operand_0_type, operand_1_type}, position + 1);

    Instruction {
        form: Form::Long,
        opcode,
        operands: operands.0,
        size: 1 + operands.1
    }
}

pub fn decode_extended_form_instruction(memory: &Memory, position: AbsoluteAddress) -> Instruction {
    let opcode = memory.at(position + 1);
    let operand_types = operand_types(memory.at(position + 2));
    let operands = decode_operands(memory, operand_types, position + 3);

    Instruction {
        form: Form::Extended,
        opcode,
        operands: operands.0,
        size: 3 + operands.1
    }
}

pub fn decode_variable_2op_instruction(memory: &Memory, position: AbsoluteAddress) -> Instruction {
    let byte = memory.at(position);
    let operand_types = operand_types(memory.at(position + 1));
    let operands = decode_operands(memory, operand_types, position + 2);
    let opcode = byte & 0b00011111;

    Instruction {
        form: Form::Variable,
        opcode,
        operands: operands.0,
        size: 2 + operands.1
    }
}

pub fn decode_variable_var_op_instruction(memory: &Memory, position: AbsoluteAddress) -> Instruction {
    let opcode = memory.at(position);
    if opcode == 236 || opcode == 250 {
        let mut operand_types_0 = operand_types(memory.at(position + 1));
        let mut operand_types_1 = operand_types(memory.at(position + 2));
        operand_types_0.append(&mut operand_types_1);
        let (operands, bytes_read) = decode_operands(memory, operand_types_0, position + 3);

        Instruction {
            form: Form::Variable,
            opcode,
            operands: operands,
            size: 3 + bytes_read
        }
    }
    else {
        let operand_types = operand_types(memory.at(position + 1));
        let (operands, bytes_read) = decode_operands(memory, operand_types, position + 2);

        Instruction {
            form: Form::Variable,
            opcode,
            operands: operands,
            size: 2 + bytes_read
        }
    }
}
