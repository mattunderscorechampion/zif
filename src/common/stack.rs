use common::address::AbsoluteAddress;
use common::misc::CallType;
use common::output::OutputStream;
use std::io::Write;
use std::io::Result;

pub struct StackFrame {
    pub routine_address: AbsoluteAddress,
    pub program_counter: AbsoluteAddress,
    pub local_variables: Vec<u16>,
    pub routine_stack: Vec<u16>,
    pub call_type: CallType,
    pub arg_count: u8,
    pub store_result: Option<u8>
}

pub struct Stack {
    pub stack: Vec<StackFrame>
}
impl Stack {
    pub fn new(address: AbsoluteAddress) -> Stack {
        let mut stack = Vec::new();
        stack.push(StackFrame {
            routine_address: address,
            program_counter: address,
            local_variables: Vec::new(),
            routine_stack: Vec::new(),
            call_type: CallType::Initial,
            arg_count: 0,
            store_result: Option::None
        });
        Stack { stack }
    }

    pub fn top(&self) -> &StackFrame {
        self.stack.last().unwrap()
    }

    pub fn top_mut(&mut self) -> &mut StackFrame {
        self.stack.last_mut().unwrap()
    }

    pub fn pop(&mut self) -> StackFrame {
        self.stack.pop().unwrap()
    }

    pub fn call_routine_and_discard_result(&mut self, body: AbsoluteAddress, argument_count: u8, variables: Vec<u16>) {
        self.stack.push(StackFrame {
            routine_address: body - 1,
            program_counter: body,
            local_variables: variables,
            routine_stack: Vec::new(),
            call_type: CallType::Procedure,
            arg_count: argument_count,
            store_result: Option::None
        });
    }

    pub fn call_routine_and_store_result(&mut self, body: AbsoluteAddress, argument_count: u8, variables: Vec<u16>, result_argument: u8) {
        self.stack.push(StackFrame {
            routine_address: body - 1,
            program_counter: body,
            local_variables: variables,
            routine_stack: Vec::new(),
            call_type: CallType::Procedure,
            arg_count: argument_count,
            store_result: Option::Some(result_argument)
        });
    }

    pub fn print_stack(&self, stream: &mut OutputStream) -> Result<()> {
        for frame in &self.stack {
            writeln!(stream, "Frame:")?;
            writeln!(stream, "Routine: {:08X}", frame.routine_address)?;
            writeln!(stream, "PC: {:08X}", frame.program_counter)?;
            writeln!(stream, "Local variables:")?;
            for (index, variable) in frame.local_variables.iter().enumerate() {
                writeln!(stream, "L{:02} {:04X}", index, variable)?;
            }
            writeln!(stream)?;
        }
        Ok(())
    }
}
