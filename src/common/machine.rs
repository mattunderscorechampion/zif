use common::address::AbsoluteAddress;
use common::input::InputStream;
use common::memory::Memory;
use common::output::OutputStream;
use std::fs::File;
use std::io::BufReader;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;
use v5;

pub enum Machine {
    V5(v5::machine::Machine)
}
impl Machine {
    fn initialise(mut file: File, verbose: OutputStream, transcript: OutputStream, is_debug: bool, breakpoints: Vec<AbsoluteAddress>) -> Result<Machine> {
        let memory = Memory::initialise_memory(&mut file)?;

        match memory.at(AbsoluteAddress(0)) {
            5 => Ok(Machine::V5(v5::machine::Machine::initialise(memory, verbose, transcript, BufReader::new(InputStream::stdin()), is_debug, breakpoints)?)),
            _ => Err(Error::new(ErrorKind::Other, "Version not supported"))
        }
    }

    pub fn run(self) {
        match self {
            Machine::V5(mut v5) => {
                loop {
                    match v5.step() {
                        Ok(con) => {
                            if !con {
                                break;
                            }
                        },
                        Err(e) => {
                            eprintln!("Failed to handle machine error {}", e);
                            break;
                        }
                    }
                }
            }
        };
    }

    pub fn print_memory(self) -> Result<()> {
        match self {
            Machine::V5(v5) => {
                v5.print_memory()
            }
        }
    }
}

pub fn create_machine(path: &String, verbose: bool, transcript_file: Option<String>, interactive: bool, breakpoints: Vec<AbsoluteAddress>) -> Result<Machine> {
    let file = File::open(path)?;
    let verbose_stream = if verbose { OutputStream::Stdout } else { OutputStream::Sink };
    let transcript_stream = if transcript_file.is_some() {
        let transcript = File::create(transcript_file.unwrap())?;
        OutputStream::File { file: transcript }
    } else {
        OutputStream::Sink
    };
    Machine::initialise(file, verbose_stream, transcript_stream, interactive, breakpoints)
}

