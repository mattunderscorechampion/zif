
pub enum CallType {
    Function,
    Procedure,
    Interrupt,
    Initial
}
