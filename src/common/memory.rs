use common::address::AbsoluteAddress;
use common::constant::header::END;
use std::fs::File;
use std::io::Error;
use std::io::ErrorKind;
use std::io::prelude::Seek;
use std::io::Read;
use std::io::Result;
use std::io::SeekFrom;
use std::mem::transmute;
use std::ops::Range;
use std::slice::from_raw_parts;
use std::slice::from_raw_parts_mut;

pub struct Memory(Vec<u8>);
impl Memory {
    pub fn initialise_memory(file: &mut File) -> Result<Memory> {
        let mut memory = Memory(Vec::new());

        file.seek(SeekFrom::Start(0))?;
        let bytes_read = file.read_to_end(&mut memory.0)?;
        if bytes_read < END {
            Err(Error::new(ErrorKind::Other, "Not enough data for header"))
        } else {
            Ok(memory)
        }
    }

    pub fn new(data: Vec<u8>) -> Memory {
        Memory(data)
    }

    pub fn at(&self, addr: AbsoluteAddress) -> u8 {
        self.0[addr.0]
    }

    pub fn set(&mut self, addr: AbsoluteAddress, byte: u8) {
        self.0[addr.0] = byte;
    }

    pub fn u16_at(&self, addr: AbsoluteAddress) -> u16 {
        let bytes : [u8; 2] = [self.0[addr.0 + 1], self.0[addr.0]];
        unsafe {
            transmute::<[u8; 2], u16>(bytes)
        }
    }

    pub fn set_uvalue<T: Into<u16>>(&mut self, addr: AbsoluteAddress, word: T) {
        self.set_u16(addr, word.into());
    }

    pub fn set_u16(&mut self, addr: AbsoluteAddress, word: u16) {
        self.0[addr.0 + 1] = (word & 0x00FF) as u8;
        self.0[addr.0] = (word >> 8) as u8;
    }

    pub fn i16_at(&self, addr: AbsoluteAddress) -> i16 {
        let bytes : [u8; 2] = [self.0[addr.0 + 1], self.0[addr.0]];
        unsafe {
            transmute::<[u8; 2], i16>(bytes)
        }
    }

    pub fn set_i16(&mut self, addr: AbsoluteAddress, word: i16) {
        self.0[addr.0 + 1] = (word & 0x00FF) as u8;
        self.0[addr.0] = (word >> 8) as u8;
    }

    pub fn set_ivalue<T: Into<i16>>(&mut self, addr: AbsoluteAddress, word: T) {
        self.set_i16(addr, word.into());
    }

    pub fn get(&self, range: Range<usize>) -> &[u8] {
        assert!(self.0.len() >= range.end);
        let ptr = self.0.as_ptr();

        unsafe {
            from_raw_parts(ptr.offset(range.start as isize), range.end)
        }
    }

    pub fn get_mut(&mut self, range: Range<usize>) -> &mut [u8] {
        assert!(self.0.len() >= range.end);
        let ptr = self.0.as_mut_ptr();

        unsafe {
            from_raw_parts_mut(ptr.offset(range.start as isize), range.end)
        }
    }

    pub fn size(&self) -> usize {
        self.0.len()
    }
}

mod tests {
    use common::memory::Memory;
    use common::address::AbsoluteAddress;

    #[test]
    fn test_access() {
        let memory = Memory(vec![0x0, 0x1, 0x2, 0x3]);

        assert_eq!(0x0, memory.at(AbsoluteAddress(0x0)));
        assert_eq!(0x1, memory.at(AbsoluteAddress(0x1)));
        assert_eq!(0x2, memory.at(AbsoluteAddress(0x2)));
        assert_eq!(0x3, memory.at(AbsoluteAddress(0x3)));
    }

    #[test]
    fn test_set() {
        let mut memory = Memory(vec![0x0, 0x1, 0x2, 0x3]);

        memory.set(AbsoluteAddress(0x0), 0x3);
        memory.set(AbsoluteAddress(0x1), 0x2);
        memory.set(AbsoluteAddress(0x2), 0x1);
        memory.set(AbsoluteAddress(0x3), 0x0);

        assert_eq!(0x3, memory.at(AbsoluteAddress(0x0)));
        assert_eq!(0x2, memory.at(AbsoluteAddress(0x1)));
        assert_eq!(0x1, memory.at(AbsoluteAddress(0x2)));
        assert_eq!(0x0, memory.at(AbsoluteAddress(0x3)));
    }

    #[test]
    fn test_u16_at() {
        let memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        assert_eq!(0x0001, memory.u16_at(AbsoluteAddress(0x0)));
        assert_eq!(0x0203, memory.u16_at(AbsoluteAddress(0x2)));
        assert_eq!(0xFFFF, memory.u16_at(AbsoluteAddress(0x4)));
    }

    #[test]
    fn test_i16_at() {
        let memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        assert_eq!(0x0001, memory.i16_at(AbsoluteAddress(0x0)));
        assert_eq!(0x0203, memory.i16_at(AbsoluteAddress(0x2)));
        assert_eq!(-0x001, memory.i16_at(AbsoluteAddress(0x4)));
    }

    #[test]
    fn test_set_u16() {
        let mut memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        memory.set_u16(AbsoluteAddress(0x0), 0xFFFF);
        memory.set_u16(AbsoluteAddress(0x2), 0x0203);
        memory.set_u16(AbsoluteAddress(0x4), 0x0001);

        assert_eq!(0x0001, memory.u16_at(AbsoluteAddress(0x4)));
        assert_eq!(0x0203, memory.u16_at(AbsoluteAddress(0x2)));
        assert_eq!(0xFFFF, memory.u16_at(AbsoluteAddress(0x0)));
    }

    #[test]
    fn test_set_i16() {
        let mut memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        memory.set_i16(AbsoluteAddress(0x0), -0x001);
        memory.set_i16(AbsoluteAddress(0x2), 0x0203);
        memory.set_i16(AbsoluteAddress(0x4), 0x0001);

        assert_eq!(0x0001, memory.i16_at(AbsoluteAddress(0x4)));
        assert_eq!(0x0203, memory.i16_at(AbsoluteAddress(0x2)));
        assert_eq!(-0x001, memory.i16_at(AbsoluteAddress(0x0)));
    }

    #[test]
    fn test_get() {
        let memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        let slice0 = memory.get(0..2);
        let slice1 = memory.get(0..3);
        let slice2 = memory.get(2..4);

        assert_eq!(0x01, slice0[0x1]);
        assert_eq!(0x01, slice1[0x1]);
        assert_eq!(0x03, slice2[0x1]);
    }

    #[test]
    fn test_get_mut() {
        let mut memory = Memory(vec![0x0, 0x1, 0x2, 0x3, 0xFF, 0xFF]);

        {
            let slice0 = memory.get_mut(0..2);
            assert_eq!(0x01, slice0[0x1]);
        }

        {
            let slice1 = memory.get_mut(0..3);
            assert_eq!(0x01, slice1[0x1]);
        }

        {
            let slice2 = memory.get_mut(2..4);
            assert_eq!(0x03, slice2[0x1]);
        }

    }
}
