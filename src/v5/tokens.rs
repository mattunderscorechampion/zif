use std::io::Result;

use common::address::{AbsoluteAddress, ToAbsoluteAddress};
use common::address::ByteAddress;
use common::memory::Memory;
use v5::dictionary::Dictionary;
use v5::header::parse_header;
use v5::z_char::V5Encoder;
use v5::z_char::ZString;

pub struct Token {
    pub start: u8,
    pub length: u8,
    pub dictionary_entry: ByteAddress
}

pub struct Tokeniser<'a, 'b> {
    dictionary: Dictionary<'a>,
    memory: &'a Memory,
    string_encoder: &'b mut V5Encoder
}

impl <'a, 'b> Tokeniser<'a, 'b> {
    pub fn new_from_header(memory: &'a Memory, string_encoder: &'b mut V5Encoder) -> Result<Tokeniser<'a, 'b>> {
        let header = parse_header(memory);
        let dictionary_address = header.dictionary.to_address();
        Tokeniser::new_for_dictionary(memory, string_encoder, dictionary_address)
    }

    pub fn new_for_dictionary(memory: &'a Memory, string_encoder: &'b mut V5Encoder, dictionary_address: AbsoluteAddress) -> Result<Tokeniser<'a, 'b>> {
        let dictionary = Dictionary::new(&memory, dictionary_address)?;
        Ok(Tokeniser {
            dictionary,
            memory,
            string_encoder
        })
    }

    fn address(&self, text_buffer: AbsoluteAddress, offset: u8) -> AbsoluteAddress {
        text_buffer + 2 + offset
    }

    fn character(&self, text_buffer: AbsoluteAddress, offset: u8) -> u8 {
        self.memory.at(self.address(text_buffer, offset))
    }

    fn string(&mut self, address: AbsoluteAddress, length: u16) -> Result<ZString> {
        self.string_encoder.encode_string_value(&self.memory, address, length)
    }

    fn entry(&mut self, text_buffer: AbsoluteAddress, offset: u8, length: u8) -> Result<ByteAddress> {
        let address = self.address(text_buffer, offset);
        let string = self.string(address, length as u16)?;
        Ok(self.dictionary.find_address(&string).unwrap_or(ByteAddress(0)))
    }

    pub fn parse_to_tokens(&mut self, text_buffer: AbsoluteAddress) -> Result<Vec<Token>> {
        let mut tokens = Vec::new();
        let separators = self.dictionary.separators();

        let mut index: u8 = 0;
        let mut token_start: u8 = 0;
        let number_of_characters = self.memory.at(text_buffer + 1);
        while index < number_of_characters {
            let character = self.character(text_buffer, index);

            if character == 32 {
                let length = index - token_start;
                let entry = self.entry(text_buffer, token_start, length)?;

                tokens.push(Token {
                    start: token_start,
                    length,
                    dictionary_entry : entry
                });
                token_start = index + 1;
            }
            else if separators.contains(&character) {
                let length = index - token_start;
                let entry = self.entry(text_buffer, token_start, length)?;

                tokens.push(Token {
                    start: token_start,
                    length,
                    dictionary_entry : entry
                });
                token_start = index + 1;

                let separator_entry = self.entry(text_buffer, index, 1)?;
                tokens.push(Token {
                    start: index,
                    length: 1,
                    dictionary_entry : separator_entry
                });
            }

            index += 1;
        }
        if token_start != index {
            let length = index - token_start;
            let entry = self.entry(text_buffer, token_start, length)?;
            tokens.push(Token {
                start: token_start,
                length: index - token_start,
                dictionary_entry : entry
            });
        }

        Ok(tokens)
    }
}