use std::fmt;
use std::fmt::Debug;
use std::fmt::Formatter;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;
use std::vec::Vec;
use std::cmp::PartialEq;

use common::address::AbsoluteAddress;
use common::memory::Memory;

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct Word(u16);
impl Word {
    fn first_char(&self) -> u8 {
        (self.0 >> 10 & 0x001F) as u8
    }

    fn second_char(&self) -> u8 {
        (self.0 >> 5 & 0x001F) as u8
    }

    fn third_char(&self) -> u8 {
        (self.0 & 0x001F) as u8
    }

    fn is_terminal(&self) -> bool {
        self.0 & 0x8000 != 0
    }

    fn set_first_char(&mut self, value: u8) {
        self.0 = self.0 & !0x001F << 10;
        self.0 = self.0 | (value as u16 & 0x001F) << 10;
    }

    fn set_second_char(&mut self, value: u8) {
        self.0 = self.0 & !0x001F << 5;
        self.0 = self.0 | (value as u16 & 0x001F) << 5;
    }

    fn set_third_char(&mut self, value: u8) {
        self.0 = self.0 & !0x001F;
        self.0 = self.0 | (value as u16 & 0x001F);
    }

    fn set_terminal(&mut self) {
        self.0 = self.0 | 0x8000;
    }

    fn clear_terminal(&mut self) {
        self.0 = self.0 & 0x7FFF;
    }

    fn is_empty(&self) -> bool {
        let first = self.first_char();
        let second = self.second_char();
        let third = self.third_char();
        (first == 5 || first == 6) && (second == 5 || second == 6) && (third == 5 || third == 6)
    }

    fn same_chars(&self, other: &Word) -> bool {
        self.0 | 0x8000 == other.0 | 0x8000
    }

    fn new(first: u8, second: u8, third: u8) -> Word {
        let mut w = Word(0);
        w.set_first_char(first);
        w.set_second_char(second);
        w.set_third_char(third);
        w
    }

    fn new_terminal(first: u8, second: u8, third: u8) -> Word {
        let mut w = Word::new(first, second, third);
        w.set_terminal();
        w
    }
}
impl Debug for Word {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        if self.is_terminal() {
            write!(f, "Word(terminal {},{},{})", self.first_char(), self.second_char(), self.third_char())
        } else {
            write!(f, "Word({},{},{})", self.first_char(), self.second_char(), self.third_char())
        }
    }
}

#[derive(Clone, Debug, Eq)]
pub struct ZString(Vec<Word>);
impl ZString {
    pub fn from_memory(memory: &Memory, address: AbsoluteAddress, length: u8) -> ZString {
        let mut current_address = address;
        let limit = address + (length * 2);
        let mut words = Vec::new();
        while current_address < limit {
            let word = Word(memory.u16_at(current_address));
            words.push(word);
            current_address += 2;
            if word.is_terminal() {
                break;
            }
        }

        ZString(words)
    }
}
impl PartialEq for ZString {
    fn eq(&self, other: &ZString) -> bool {
        let mut self_words = self.0.iter();
        let mut self_other = other.0.iter();

        loop {
            match self_words.next() {
                Some(self_word) => {
                    match self_other.next() {
                        Some(other_word) => {
                            if !self_word.same_chars(other_word) {
                                return false;
                            }
                        },
                        None => {
                            return self_word.is_empty();
                        }
                    }
                },
                None => {
                    match self_other.next() {
                        Some(other_word) => {
                            return other_word.is_empty();
                        },
                        None => {
                            return true;
                        }
                    }
                }
            }
        }
    }
}
impl IntoIterator for ZString {
    type Item = Word;
    type IntoIter = ::std::vec::IntoIter<Word>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

mod zchar_tests {
    use v5::z_char::Word;
    use v5::z_char::ZString;

    #[test]
    fn set_and_get() {
        let mut word = Word(0);

        word.set_first_char(10);
        word.set_second_char(20);
        word.set_third_char(30);
        word.set_terminal();
        assert!(word.is_terminal());

        word.clear_terminal();
        assert!(!word.is_terminal());
        assert_eq!(10, word.first_char());
        assert_eq!(20, word.second_char());
        assert_eq!(30, word.third_char());
    }

    #[test]
    fn string_equality() {
        let about0 = ZString(vec![
            Word::new(6, 7, 20),
            Word::new_terminal(26, 25, 5)]);
        let about1 = ZString(vec![
            Word::new(6, 7, 20),
            Word::new_terminal(26, 25, 5)]);
        let about2 = ZString(vec![
            Word::new(6, 7, 20),
            Word::new(26, 25, 5),
            Word::new_terminal(5, 5, 5)]);
        let look = ZString(vec![
            Word::new(17, 20, 20),
            Word::new_terminal(16, 5, 5)]);

        assert_eq!(about0, about1);
        assert_eq!(about0, about2);
        assert_eq!(about1, about0);
        assert_eq!(about1, about2);
        assert_eq!(about2, about0);
        assert_eq!(about2, about1);

        assert_ne!(look, about0);
        assert_ne!(look, about1);
        assert_ne!(look, about2);
    }
}

pub struct AlphabetTable {
    a0: [char; 32],
    a1: [char; 32],
    a2: [char; 32]
}

pub const DEFAULT_ALPHABET_V2: AlphabetTable = AlphabetTable {
    a0: [' ', ' ', ' ', ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    a1: [' ', ' ', ' ', ' ', ' ', ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
    a2: [' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '!', '?', '_', '#', '\'', '"', '/', '\\', '-', ':', '(', ')'],
};

#[derive(Debug, Copy, Clone)]
pub enum Alphabet {
    A0,
    A1,
    A2,
}
impl Alphabet {
    fn up(&self) -> Alphabet {
        match self {
            Alphabet::A0 => Alphabet::A1,
            Alphabet::A1 => Alphabet::A2,
            Alphabet::A2 => Alphabet::A0
        }
    }

    fn down(&self) -> Alphabet {
        match self {
            Alphabet::A0 => Alphabet::A2,
            Alphabet::A1 => Alphabet::A0,
            Alphabet::A2 => Alphabet::A1
        }
    }
}

enum State {
    Normal,
    StartZChar,
    ContinueZChar(u8)
}

pub struct V5Decoder {
    alphabet_table: AlphabetTable,
    current_alphabet: Alphabet,
    lock_alphabet: Alphabet,
    state: State
}

impl V5Decoder {
    pub fn new(alphabet_table: AlphabetTable) -> V5Decoder {
        V5Decoder {
            alphabet_table,
            current_alphabet: Alphabet::A0,
            lock_alphabet: Alphabet::A0,
            state: State::Normal
        }
    }

    fn normal_decode_character(&mut self, zchar: u8, destination: &mut String) -> Result<()> {
        if zchar == 0 {
            destination.push(' ');
        } else if zchar > 0 && zchar < 4 {
            return Err(Error::new(ErrorKind::Other, "Abbreviations not yet supported"));
        } else if zchar == 4 {
            self.current_alphabet = self.current_alphabet.up();
        } else if zchar == 5 {
            self.current_alphabet = self.current_alphabet.down();
        } else {
            let result = match self.current_alphabet {
                Alphabet::A0 if zchar >= 6 => { Option::Some(self.alphabet_table.a0[zchar as usize]) },
                Alphabet::A1 if zchar >= 6 => { Option::Some(self.alphabet_table.a1[zchar as usize]) },
                Alphabet::A2 if zchar == 6 => { self.state = State::StartZChar; Option::None },
                Alphabet::A2 if zchar >= 7 => { Option::Some(self.alphabet_table.a2[zchar as usize]) },
                _ => return Err(Error::new(ErrorKind::Other, format!("Unable to decode zchar {:X?}", zchar)))
            };
            match result {
                Some(c) => destination.push(c),
                None => {}
            }
            // Revert alphabet shift
            self.current_alphabet = self.lock_alphabet;
        }
        Ok(())
    }

    fn decode_character(&mut self, zchar: u8, destination: &mut String) -> Result<()> {
        match self.state {
            State::StartZChar => { self.state = State::ContinueZChar(zchar & 0x1F); Ok(()) },
            State::ContinueZChar(top) => {
                let bottom = (zchar & 0x1F) as u16;
                let zscii = bottom + ((top as u16) << 5);
                destination.push(self.decode_zscii(zscii)?);
                self.state = State::Normal;
                Ok(())
            },
            State::Normal => { self.normal_decode_character(zchar, destination) }
        }
    }

    pub fn decode_zscii(&mut self, zscii: u16) -> Result<char> {
        if zscii == 13 {
            Ok('\n')
        }
        else if zscii >= 32 && zscii <= 126 {
            Ok(zscii as u8 as char)
        }
        else {
            Err(Error::new(ErrorKind::Other, format!("Unable to decode zscii {:X?}", zscii)))
        }
    }

    pub fn decode_string(&mut self, memory: &Memory, starting_address: AbsoluteAddress) -> Result<(String, usize)> {
        let mut string = String::new();
        let mut bytes_read = 0;
        loop {
            let word_address: AbsoluteAddress = starting_address + bytes_read;
            if memory.size() < word_address.0 + 1 {
                break;
            }

            let word = Word(memory.u16_at(word_address));
            bytes_read += 2;

            self.decode_character(word.first_char(), &mut string)?;
            self.decode_character(word.second_char(), &mut string)?;
            self.decode_character(word.third_char(), &mut string)?;

            if word.is_terminal() {
                break;
            }
        }
        // Revert alphabet shift
        self.current_alphabet = self.lock_alphabet;
        Ok((string, bytes_read))
    }
}

enum EncodedCharacter {
    A0(u8),
    A1(u8),
    A2(u8)
}

pub struct V5Encoder {
    alphabet_table: AlphabetTable,
    current_alphabet: Alphabet,
    lock_alphabet: Alphabet
}

impl V5Encoder {
    pub fn new(alphabet_table: AlphabetTable) -> V5Encoder {
        V5Encoder {
            alphabet_table,
            current_alphabet: Alphabet::A0,
            lock_alphabet: Alphabet::A0,
        }
    }

    fn find_char_in_alphabet(character: char, alphabet_table: &[char; 32]) -> Option<u8> {
        let mut offset: u8 = 0;
        for current_char in alphabet_table.iter() {
            if character == *current_char {
                return Some(offset);
            }
            offset += 1;
        }
        None
    }

    fn encode_character(&mut self, character: char) -> Result<EncodedCharacter> {
        let maybe_char_0: Option<u8> = V5Encoder::find_char_in_alphabet(character, &self.alphabet_table.a0);
        if maybe_char_0.is_some() {
            return Ok(EncodedCharacter::A0(maybe_char_0.unwrap()));
        }

        let maybe_char_1: Option<u8> = V5Encoder::find_char_in_alphabet(character, &self.alphabet_table.a1);
        if maybe_char_1.is_some() {
            return Ok(EncodedCharacter::A1(maybe_char_1.unwrap()));
        }

        let maybe_char_2: Option<u8> = V5Encoder::find_char_in_alphabet(character, &self.alphabet_table.a2);
        if maybe_char_2.is_some() {
            return Ok(EncodedCharacter::A2(maybe_char_2.unwrap()));
        }

        Err(Error::new(ErrorKind::Other, format!("Unable to locate character in alphabet {:?}", character)))
    }

    pub fn encode_string_value(
        &mut self,
        memory: &Memory,
        starting_address: AbsoluteAddress,
        length: u16) -> Result<ZString> {

        let mut words: Vec<Word> = Vec::new();
        let mut current_word = Word(0);
        let mut position = 0;
        let mut offset = 0;
        loop {
            let character = memory.at(starting_address + position) as char;

            let zchar = self.encode_character(character)?;

            match zchar {
                EncodedCharacter::A0(v) => {
                    if offset == 0 {
                        current_word.set_first_char(v);
                        offset = (offset + 1) % 3;
                    } else if offset == 1 {
                        current_word.set_second_char(v);
                        offset = (offset + 1) % 3;
                    } else {
                        current_word.set_third_char(v);
                        words.push(current_word);
                        current_word = Word(0);
                        offset = (offset + 1) % 3;
                    }
                },
                EncodedCharacter::A1(v) => {
                    if offset == 0 {
                        current_word.set_first_char(4);
                        current_word.set_second_char(v);
                        offset = (offset + 2) % 3;
                    } else if offset == 1 {
                        current_word.set_second_char(4);
                        current_word.set_third_char(v);
                        words.push(current_word);
                        current_word = Word(0);
                        offset = (offset + 2) % 3;
                    } else {
                        current_word.set_third_char(4);
                        words.push(current_word);
                        current_word = Word(0);
                        current_word.set_first_char(v);
                        offset = (offset + 2) % 3;
                    }
                },
                EncodedCharacter::A2(v) => {
                    if offset == 0 {
                        current_word.set_first_char(5);
                        current_word.set_second_char(v);
                        offset = (offset + 2) % 3
                    } else if offset == 1 {
                        current_word.set_second_char(5);
                        current_word.set_third_char(v);
                        words.push(current_word);
                        current_word = Word(0);
                        offset = (offset + 2) % 3
                    } else {
                        current_word.set_third_char(5);
                        words.push(current_word);
                        current_word = Word(0);
                        current_word.set_first_char(v);
                        offset = (offset + 2) % 3
                    }
                },
            }

            self.current_alphabet = self.lock_alphabet;

            position += 1;
            if length <= position {
                break;
            }
        }

        if offset == 0 {
            let last_word = words.last_mut();
            if last_word.is_some() {
                last_word.unwrap().set_terminal();
            }
        } else if offset == 1 {
            current_word.set_terminal();
            current_word.set_second_char(5);
            current_word.set_third_char(5);
            words.push(current_word);
        } else {
            current_word.set_terminal();
            current_word.set_third_char(5);
            words.push(current_word);
        }

        Ok(ZString(words))
    }

    pub fn encode_string(
        &mut self,
        memory: &mut Memory,
        starting_address: AbsoluteAddress,
        length: u16,
        output_buffer: AbsoluteAddress) -> Result<()> {

        let string = self.encode_string_value(memory, starting_address, length)?;

        let mut target_address = output_buffer;
        for word in string {
            memory.set_u16(target_address, word.0);
            target_address += 2;
        }

        Ok(())
    }
}

mod z_char_tests {
    use common::address::AbsoluteAddress;
    use common::memory::Memory;
    use v5::z_char::DEFAULT_ALPHABET_V2;
    use v5::z_char::V5Decoder;
    use v5::z_char::V5Encoder;

    #[test]
    fn round_trip() {
        let mut encoder = V5Encoder::new(DEFAULT_ALPHABET_V2);
        let mut decoder = V5Decoder::new(DEFAULT_ALPHABET_V2);
        let unencoded_string = AbsoluteAddress(0);
        let mut encoded_string = AbsoluteAddress(1);
        let mut data = Vec::new();
        let mut length = 0;
        for byte in "Hello, world!".bytes() {
            data.push(byte);
            encoded_string = encoded_string + 1;
            length += 1;
        }
        data.append(&mut vec![0; 64]);
        let mut memory = Memory::new(data);

        encoder.encode_string(&mut memory, unencoded_string, length as u16, encoded_string).unwrap();
        let result = decoder.decode_string(&mut memory, encoded_string).unwrap().0;

        assert_eq!(result, "Hello, world!");
    }

    #[test]
    fn bounded_length() {
        let mut encoder = V5Encoder::new(DEFAULT_ALPHABET_V2);
        let mut decoder = V5Decoder::new(DEFAULT_ALPHABET_V2);
        let unencoded_string = AbsoluteAddress(0);
        let mut encoded_string = AbsoluteAddress(1);
        let mut data = Vec::new();
        for byte in "Hello, world!".bytes() {
            data.push(byte);
            encoded_string = encoded_string + 1;
        }
        data.append(&mut vec![0; 64]);
        let mut memory = Memory::new(data);

        encoder.encode_string(&mut memory, unencoded_string, 5, encoded_string).unwrap();
        let result = decoder.decode_string(&mut memory, encoded_string).unwrap().0;

        assert_eq!(result, "Hello");
    }
}
