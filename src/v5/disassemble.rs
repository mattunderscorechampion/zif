use std::cmp::max;
use std::collections::BTreeMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;
use std::io::Write;
use std::mem::transmute;

use common::address::AbsoluteAddress;
use common::address::ToAbsoluteAddress;
use common::branch::BranchArgument;
use common::memory::Memory;
use common::operand::Operand;
use common::output::OutputStream;
use v5::address::PackedAddress;
use v5::header::parse_header;
use v5::instruction::{decode_instruction, is_jump};
use v5::instruction::Instruction;
use v5::instruction::is_branch;
use v5::instruction::is_call;
use v5::instruction::is_quit;
use v5::instruction::is_return;
use v5::routine::decode_routine;
use v5::routine::Routine;
use v5::z_char::DEFAULT_ALPHABET_V2;
use v5::z_char::V5Decoder;

fn get_constant(instruction: &Instruction, idx: u8) -> Option<u16> {
    let operand: &Operand = match instruction.operands.get(idx as usize) {
        None => return Option::None,
        Some(v) => v
    };

    match operand {
        &Operand::LargeConstant(v) => Option::Some(v),
        &Operand::SmallConstant(v) => Option::Some(v as u16),
        _ => Option::None
    }
}

fn get_signed_constant(instruction: &Instruction, idx: u8) -> Option<i16> {
    match get_constant(instruction, idx) {
        Some(unsigned_value) => {
            Option::Some(unsafe { transmute::<u16, i16>(unsigned_value) })
        },
        None => Option::None
    }
}

fn next_packed_address(address: AbsoluteAddress) -> Result<PackedAddress> {
    let offset = address.0 % 4;
    if offset == 0 {
        PackedAddress::from_address(address)
    } else if offset == 1 {
        PackedAddress::from_address(address + 3)
    } else if offset == 2 {
        PackedAddress::from_address(address + 2)
    } else {
        PackedAddress::from_address(address + 1)
    }
}

enum RoutineInfo {
    Ok {
        routine: Routine,
        end: AbsoluteAddress,
        callers: HashSet<PackedAddress>
    },
    Bad {
        callers: HashSet<PackedAddress>,
        reached: Option<AbsoluteAddress>,
        error: Error
    }
}

struct StringInfo {
    value: String,
    length: usize
}

pub struct Disassembler {
    memory: Memory,
    decoder: V5Decoder,
    routines: BTreeMap<PackedAddress, RoutineInfo>,
    strings: BTreeMap<AbsoluteAddress, StringInfo>
}

impl Disassembler {
    fn new(memory: Memory) -> Disassembler {
        Disassembler {
            memory,
            decoder: V5Decoder::new(DEFAULT_ALPHABET_V2),
            routines: BTreeMap::new(),
            strings: BTreeMap::new()
        }
    }

    pub fn find_routines(&mut self) -> Result<()> {
        let header = parse_header(&self.memory);
        let main_address = header.initial_program_counter;

        self.append_routine(PackedAddress::from_address(main_address.to_address() - 1)?, Option::None)
    }

    fn find_strings(&mut self) -> Result<()> {
        let end_of_code: Option<AbsoluteAddress> = self
            .routines
            .values()
            .fold(
                None,
                |found: Option<AbsoluteAddress>, entry: &RoutineInfo| match entry {
                    RoutineInfo::Ok { routine: _, end, callers: _ } => match found {
                        None => Some(*end),
                        Some(found_address) => if found_address < *end { Some(*end) } else { Some(found_address) }
                    },
                    RoutineInfo::Bad { callers: _, reached: _, error: _ } => found
                });

        let end_of_routines = end_of_code.ok_or(Error::new(ErrorKind::Other, "No strings found"))?;
        let mut start_of_string = next_packed_address(end_of_routines)?.to_address();

        while start_of_string.to_address() + 1 < self.memory.size() as usize {
            match self.decoder.decode_string(&self.memory, start_of_string) {
                Ok((value, length)) => {
                    self.strings.insert(start_of_string, StringInfo { value, length });
                    start_of_string = next_packed_address(start_of_string + length)?.to_address();
                },
                Err(_e) => { return Ok(()); }
            }
        }

        Ok(())
    }

    fn append_routine(&mut self, address: PackedAddress, caller: Option<PackedAddress>) -> Result<()> {
        // Check for existing routine record
        match self.routines.get_mut(&address) {
            Option::Some(routine_info) => {
                match caller {
                    Option::Some(c) => {
                        // Update routine with caller information
                        match routine_info {
                            RoutineInfo::Ok { routine: _, end: _, callers } => { callers.insert(c); },
                            RoutineInfo::Bad { callers, reached: _, error: _ } => { callers.insert(c); }
                        };
                    },
                    Option::None => {}
                };
                return Ok(());
            },
            Option::None => {}
        }

        // Check the routine exists in memory
        if address.to_address() > self.memory.size() {
            let mut callers = HashSet::new();
            match caller {
                Some(c) => { callers.insert(c); },
                None => {}
            }
            self.routines.insert(address, RoutineInfo::Bad {
                callers,
                reached: None,
                error: Error::new(ErrorKind::Other, "Address out of memory")
            });
            return Ok(());
        }

        let routine = match decode_routine(&self.memory, address) {
            Ok(routine) => {
                routine
            },
            Err(e) => {
                let mut callers = HashSet::new();
                match caller {
                    Some(c) => { callers.insert(c); },
                    None => {}
                }
                self.routines.insert(address, RoutineInfo::Bad {
                    callers,
                    reached: None,
                    error: e
                });
                return Ok(());
            }
        };

        let (end, error) = self.find_routine_end(&routine);

        if error.is_none() {
            let start = routine.body;
            let mut callers = HashSet::new();
            match caller {
                Some(c) => { callers.insert(c); },
                None => {}
            }

            // Create the record for the routine
            self.routines.insert(address, RoutineInfo::Ok { routine, end, callers });

            // Search the routine for any called routines
            let mut instruction_address = start;
            loop {
                match decode_instruction(&self.memory, &mut self.decoder, instruction_address) {
                    Ok(instruction) => {
                        if instruction_address > end {
                            break;
                        } else if is_call(&instruction) {
                            match get_constant(&instruction, 0) {
                                Some(addr) => self.append_routine(PackedAddress(addr), Option::Some(address))?,
                                None => {}
                            };
                        }

                        instruction_address += instruction.size;
                    },
                    Err(_e) => break
                };
            }

            // Attempt to decode next packed address as routine
            let next_packed_address = next_packed_address(instruction_address);
            match next_packed_address {
                Ok(next_address) => {
                    if next_address.to_address() < self.memory.size() {
                        self.append_routine(next_address, None)?;
                    }
                },
                _ => {}
            }
        } else {
            let mut callers = HashSet::new();
            match caller {
                Some(c) => { callers.insert(c); },
                None => {}
            }
            self.routines.insert(address, RoutineInfo::Bad {
                callers,
                reached: Some(end),
                error: error.unwrap()
            });
        }

        Ok(())
    }

    fn find_routine_end(&mut self, routine: &Routine) -> (AbsoluteAddress, Option<Error>) {
        let mut high_instruction =  routine.body;
        let mut instruction_address = routine.body;
        loop {
            if instruction_address >= self.memory.size() {
                return (high_instruction, Some(Error::new(ErrorKind::Other, format!("The address {:08X?} is outside the memory bounds", instruction_address))))
            }
            else if instruction_address + 8 >= self.memory.size() {
                return (high_instruction, Some(Error::new(ErrorKind::Other, format!("The address {:08X?} is too close to the memory bounds", instruction_address))))
            }

            let instruction = match decode_instruction(&self.memory, &mut self.decoder, instruction_address) {
                Ok(instruction) => instruction,
                Err(e) => {
                    return (high_instruction, Some(e));
                }
            };
            if is_return(&instruction) || is_quit(&instruction) {
                if high_instruction <= instruction_address {
                    break;
                }
                else {
                    instruction_address += instruction.size;
                }
            } else if is_jump(&instruction) {
                match get_signed_constant(&instruction, 0) {
                    Some(offset) => {
                        if offset < 0 {
                            if high_instruction <= instruction_address {
                                break;
                            }
                            else {
                                instruction_address += instruction.size;
                            }
                        }
                        else {
                            instruction_address += instruction.size;
                            high_instruction = max(high_instruction, instruction_address + offset as usize - 2);
                        }
                    }
                    _ => {}
                }
            } else if is_branch(&instruction) {
                match instruction.branch.unwrap() {
                    BranchArgument::ReturnTrue { reverse_logic: _, resume_address } => instruction_address = resume_address,
                    BranchArgument::ReturnFalse { reverse_logic: _, resume_address } => instruction_address = resume_address,
                    BranchArgument::Jump { reverse_logic: _, target_address, resume_address } => {
                        instruction_address = resume_address;
                        high_instruction = max(high_instruction, max(resume_address, target_address));
                    },
                }
            } else {
                instruction_address += instruction.size;
                high_instruction = max(high_instruction, instruction_address);
            }
        }
        (instruction_address, None)
    }

    pub fn write_routines(&mut self, mut output: OutputStream) -> Result<()> {
        writeln!(output, "{} routines", self.routines.len())?;
        writeln!(output)?;

        for (routine_address, routine_info) in &self.routines {

            match routine_info {
                RoutineInfo::Ok { routine, end, callers } => {
                    writeln!(output, "Routine {:08X}-{:08X}, {} locals", &routine_address.to_address(), end, routine.number_of_local_variables)?;
                    write!(output, "Callers ")?;
                    for caller in callers {
                        write!(output, "{:08X} ", caller)?;
                    }
                    writeln!(output)?;
                    writeln!(output)?;
                    let mut instruction_address = routine.body;
                    while instruction_address <= *end {
                        match decode_instruction(&self.memory, &mut self.decoder, instruction_address) {
                            Ok(instruction) => {
                                writeln!(output, "{:08X} {}", instruction_address, instruction)?;
                                instruction_address += instruction.size;
                            },
                            Err(e) => {
                                writeln!(output, "{:08X} Error {}", instruction_address, e)?;
                                break;
                            }
                        }

                    }
                    writeln!(output)?;
                },
                RoutineInfo::Bad { callers, reached , error} => {
                    writeln!(output, "Routine {:08X}", &routine_address.to_address())?;
                    write!(output, "Callers ")?;
                    for caller in callers {
                        write!(output, "{:08X} ", caller)?;
                    }
                    writeln!(output)?;
                    if reached.is_some() {
                        writeln!(output)?;
                        let mut instruction_address = routine_address.to_address() + 1;
                        while instruction_address <= reached.unwrap() {
                            match decode_instruction(&self.memory, &mut self.decoder, instruction_address) {
                                Ok(instruction) => {
                                    writeln!(output, "{:08X} {}", instruction_address, instruction)?;
                                    instruction_address += instruction.size;
                                },
                                Err(e) => {
                                    writeln!(output, "{:08X} Error {}", instruction_address, e)?;
                                    break;
                                }
                            }

                        }
                    }

                    writeln!(output)?;
                    writeln!(output, "Failed to decode routine")?;
                    writeln!(output, "{}", error)?;
                    writeln!(output)?;
                }
            }
        }
        Ok(())
    }

    fn write_strings(&mut self, mut output: OutputStream) -> Result<()> {
        writeln!(output, "{} strings", self.strings.len())?;
        writeln!(output)?;

        let mut count = 0;
        for (string_address, string_info) in &self.strings {
            writeln!(output, "{:08X} {:04} {}", string_address, count, string_info.value)?;
            count += 1;
        }

        Ok(())
    }
}

pub fn create_disassembler(path: &String) -> Result<Disassembler> {
    let mut file = File::open(path)?;
    let memory = Memory::initialise_memory(&mut file)?;
    Ok(Disassembler::new(memory))
}

pub fn print_disassembly(path: &String) -> Result<()> {
    let mut disassembler = create_disassembler(path)?;
    disassembler.find_routines()?;
    disassembler.write_routines(OutputStream::Stdout)?;
    disassembler.find_strings()?;
    disassembler.write_strings(OutputStream::Stdout)
}
