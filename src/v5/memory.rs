use std::cmp::min;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;
use std::io::Write;
use std::num::Wrapping;

use common::address::AbsoluteAddress;
use common::address::ToAbsoluteAddress;
use common::constant::header::END;
use common::memory::Memory;
use common::output::OutputStream;
use v5::dictionary::Dictionary;
use v5::header::parse_header;
use v5::header::StaticHeader;
use v5::objects::{AccessObject, AccessObjects, ObjectId, ObjectTableView, AccessProperties, PropertyMetadata};
use v5::z_char::DEFAULT_ALPHABET_V2;
use v5::z_char::V5Decoder;
use std::borrow::Borrow;

pub fn calculate_checksum(memory: &Memory, header: &StaticHeader) -> u16 {
    let mut checksum: Wrapping<u16> = Wrapping(0 as u16);
    let file_length = header.file_length();
    for i in END..file_length {
        checksum = checksum + Wrapping(memory.at(AbsoluteAddress(i)) as u16);
    }
    checksum.0
}

pub fn validate_with_checksum(memory: &Memory, header: &StaticHeader) -> Result<()> {
    let checksum = calculate_checksum(memory, header);

    if checksum != header.checksum {
        Err(Error::new(ErrorKind::Other, "Checksum mismatch"))
    } else {
        Ok(())
    }
}

fn print_hex_range(writer: &mut OutputStream, memory: &Memory, start_address: AbsoluteAddress, end_address: AbsoluteAddress) -> Result<()> {
    let mut addr = start_address;
    while addr < end_address {
        write!(writer, "{:07X}", addr)?;
        let line_limit = addr + 0x10;
        while addr < line_limit && addr < end_address {
            write!(writer, " {:02x}", memory.at(addr))?;
            addr += 1;
        }
        writeln!(writer)?;
    }
    Ok(())
}

pub fn describe_memory(mut writer: OutputStream, memory: &Memory) -> Result<()> {
    let header = parse_header(memory);
    writeln!(writer, "Header:")?;
    writeln!(writer, "Version: {}", header.version)?;
    writeln!(writer, "File length: 0x{:X}", header.file_length)?;
    writeln!(writer, "Checksum: 0x{:X}", header.checksum)?;
    writeln!(writer, "Initial program counter: 0x{:07X}", header.initial_program_counter)?;
    writeln!(writer, "Global variables: 0x{:07X}-0x{:07X}", header.global_variables, header.global_variables.to_address() + 480)?;
    writeln!(writer, "Object table: 0x{:07X}", header.object_table)?;
    writeln!(writer)?;

    let mut string_decoder = V5Decoder::new(DEFAULT_ALPHABET_V2);
    let header = parse_header(memory);
    let dictionary_address = header.dictionary.to_address();
    let dictionary = Dictionary::new(memory, dictionary_address)?;

    writeln!(writer, "Dictionary: 0x{:07X}", &dictionary_address)?;
    writeln!(writer, "{} entries", dictionary.number_of_entries)?;
    writeln!(writer, "Entry size {} bytes", dictionary.entry_size)?;

    writeln!(writer, "Separators:")?;
    for separator in dictionary.separators() {
        writeln!(writer, "    {}", separator as char)?;
    }

    writeln!(writer, "Entries:")?;
    for entry in dictionary.into_iter() {
        let v = string_decoder.decode_string(memory, entry.address)?;
        writeln!(writer, "    {}", v.0)?;
    }
    writeln!(writer)?;

    let object_table = ObjectTableView {
        memory,
        start_of_object_table: header.object_table.to_address()
    };
    writeln!(writer, "{:?}", object_table)?;
    writeln!(writer)?;

    {
        let static_memory = header.static_memory.to_address();
        let start_address = AbsoluteAddress(END);
        writeln!(writer, "Dynamic Memory: 0x{:07X}-0x{:07X}", start_address, static_memory)?;
        print_hex_range(&mut writer, memory, start_address, static_memory)?;
    }
    writeln!(writer)?;

    {
        writeln!(writer, "Static memory: 0x{:07X}", header.static_memory)?;
        let limit = AbsoluteAddress(min(header.file_length(), 0x0fff0));
        print_hex_range(&mut writer, memory, header.static_memory.to_address(), limit)?;
    }
    writeln!(writer)?;

    {
        writeln!(writer, "High memory: 0x{:07X}", header.high_memory.to_address())?;
        print_hex_range(&mut writer, memory, header.high_memory.to_address(), AbsoluteAddress(header.file_length()))?;
    }
    writeln!(writer)
}
