use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;

use common::address::AbsoluteAddress;
use common::memory::Memory;
use v5::z_char::ZString;
use common::address::ByteAddress;

pub struct Dictionary<'a> {
    memory: &'a Memory,
    starting_address: AbsoluteAddress,
    pub entry_size: u8,
    pub number_of_entries: u16,
    sorted: bool,
    entries_address: AbsoluteAddress
}

pub struct DictionaryEntry {
    pub address: AbsoluteAddress,
    pub string: ZString
}

pub struct DictionaryIterator<'a> {
    dictionary: Dictionary<'a>,
    position: AbsoluteAddress,
    limit: AbsoluteAddress
}
impl <'a> Iterator for DictionaryIterator<'a> {
    type Item = DictionaryEntry;

    fn next(&mut self) -> Option<DictionaryEntry> {
        if self.position >= self.limit {
            return Option::None;
        }

        let entry = DictionaryEntry {
            address: self.position,
            string: ZString::from_memory(self.dictionary.memory, self.position, self.dictionary.entry_size)
        };

        self.position += self.dictionary.entry_size;

        Option::Some(entry)
    }
}

impl <'a> Dictionary<'a> {
    pub fn new(memory: &'a Memory, starting_address: AbsoluteAddress) -> Result<Dictionary<'a>> {
        let separator_entries = memory.at(starting_address);
        let entry_size = memory.at(starting_address + separator_entries + 1);
        let entries = memory.i16_at(starting_address + separator_entries + 2);
        let sorted = entries >= 0;
        let number_of_entries = if sorted { entries as u16 } else { -entries as u16 };
        let entries_address = starting_address + separator_entries + 4;

        if entry_size < 6 {
            Err(Error::new(ErrorKind::Other, format!("Dictionary entry size {} should be greater than 5", entry_size)))
        } else {
            Ok(Dictionary {
                memory,
                starting_address,
                entry_size,
                number_of_entries,
                sorted,
                entries_address
            })
        }
    }

    pub fn separators(&self) -> Vec<u8> {
        let mut separators = Vec::new();

        let separator_entries = self.memory.at(self.starting_address);
        for index in 0..separator_entries {
            separators.push(self.memory.at(self.starting_address + 1 + index));
        }

        separators
    }

    pub fn find(&self, zstring: &ZString) -> Option<u8> {
        let mut offset = 0;
        let size_of_dictionary = self.entry_size as usize * self.number_of_entries as usize;
        let limit = self.entries_address + size_of_dictionary;
        let mut position = self.entries_address;
        while self.entries_address + (self.entry_size * offset) < limit {
            if ZString::from_memory(self.memory, position, self.entry_size) == *zstring {
                return Option::Some(offset)
            }

            position += self.entry_size;
            offset += 1;
        }

        Option::None
    }

    pub fn find_address(&self, zstring: &ZString) -> Option<ByteAddress> {
        let size_of_dictionary = self.entry_size as usize * self.number_of_entries as usize;
        let limit = self.entries_address + size_of_dictionary;
        let mut position = self.entries_address;
        while position < limit {
            if ZString::from_memory(self.memory, position, self.entry_size) == *zstring {
                return Option::Some(ByteAddress(position.0 as u16));
            }

            position += self.entry_size;
        }

        Option::None
    }
}
impl <'a> IntoIterator for Dictionary<'a> {
    type Item = DictionaryEntry;
    type IntoIter = DictionaryIterator<'a>;

    fn into_iter(self) -> DictionaryIterator<'a> {
        let size_of_dictionary = self.entry_size as usize * self.number_of_entries as usize;
        let limit = self.entries_address + size_of_dictionary;
        let position = self.entries_address;

        DictionaryIterator {
            dictionary: self,
            position,
            limit
        }
    }
}
