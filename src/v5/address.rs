use common::address::AbsoluteAddress;
use common::address::ToAbsoluteAddress;
use std::fmt;
use std::fmt::Formatter;
use std::fmt::UpperHex;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct PackedAddress(pub u16);
impl PackedAddress {
    pub fn from_address<T: ToAbsoluteAddress>(to_address: T) -> Result<PackedAddress> {
        let address = to_address.to_address();
        if address.0 % 4 != 0 {
            Err(Error::new(ErrorKind::Other, format!("Address {:?} not valid packed address", address)))
        }
        else {
            Ok(PackedAddress((address.0 / 4) as u16))
        }
    }
}
impl ToAbsoluteAddress for PackedAddress {
    fn to_address(&self) -> AbsoluteAddress {
        AbsoluteAddress(self.0 as usize * 4)
    }
}
impl UpperHex for PackedAddress {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let width = f.width().unwrap_or(0);
        let format = format!("{:.*X}", width, self.to_address());
        f.pad_integral(true, "0", &format)
    }
}
