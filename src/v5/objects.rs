use std::borrow::{Borrow, BorrowMut};

use common::address::AbsoluteAddress;
use common::address::ByteAddress;
use common::address::ToAbsoluteAddress;
use common::memory::Memory;
use std::fmt::{Debug, Formatter, Error};
use v5::z_char::{V5Decoder, DEFAULT_ALPHABET_V2};

const NULL_OBJECT: ObjectId = ObjectId(0);

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct ObjectId(pub u16);
impl Into<u16> for ObjectId {
    fn into(self) -> u16 {
        self.0
    }
}

const PARENT_OFFSET: u8 = 6;
const SIBLING_OFFSET: u8 = 8;
const CHILD_OFFSET: u8 = 10;
const PROPERTIES_OFFSET: u8 = 12;
const OBJECT_ENTRIES_OFFSET: u8 = 126;
const OBJECT_ENTRY_LENGTH: usize = 14;

pub trait AccessObjects : Borrow<Memory> + Borrow<AbsoluteAddress> {
    fn u16_at(&self, offset: u16) -> u16 {
        let start_of_object_table: &AbsoluteAddress = self.borrow();
        let memory: &Memory = self.borrow();
        memory.u16_at(*start_of_object_table + offset as usize)
    }

    fn get_default_property_value(&self, property: u8) -> u16 {
        self.u16_at((property - 1) as u16 * 2)
    }

    fn get_object_address(&self, object: ObjectId) -> AbsoluteAddress {
        let start_of_object_table: &AbsoluteAddress = self.borrow();
        *start_of_object_table + OBJECT_ENTRIES_OFFSET + ((object.0 - 1) as usize * OBJECT_ENTRY_LENGTH)
    }

    fn get_object(&self, object: ObjectId) -> ObjectView {
        let object_address = { self.get_object_address(object) };
        let memory: &Memory = self.borrow();
        ObjectView {
            memory,
            start_of_object: object_address
        }
    }
}

pub trait ModifyObjects : AccessObjects + BorrowMut<Memory> + BorrowMut<AbsoluteAddress> {
    fn insert_obj(&mut self, object: ObjectId, destination: ObjectId) {
        let object_address = self.get_object_address(object);
        let destination_address = self.get_object_address(destination);
        let source = {
            let memory: &Memory = self.borrow_mut();
            ObjectId(memory.u16_at(object_address + PARENT_OFFSET))
        };

        if source != NULL_OBJECT {
            // Update the remaining children of the old parent
            let source_address = self.get_object_address(source);

            let (source_child, sibling) = {
                let memory: &Memory = self.borrow_mut();
                let source_child = ObjectId(memory.u16_at(source_address + CHILD_OFFSET));
                let sibling = memory.u16_at(object_address + SIBLING_OFFSET);
                (source_child, sibling)
            };
            if source_child == object {
                // Update the old parent's child
                let memory: &mut Memory = self.borrow_mut();
                memory.set_u16(source_address + CHILD_OFFSET, sibling);
            } else {
                // Find the sibling before the moved object and set the next
                // sibling to the object after the moved object
                let mut current_address = self.get_object_address(source_child);
                let mut next_child = {
                    let memory: &Memory = self.borrow_mut();
                    ObjectId(memory.u16_at(current_address + SIBLING_OFFSET))
                };
                loop {
                    if next_child == NULL_OBJECT {
                        panic!("Bad object tree")
                    } else if next_child == object {
                        let memory: &mut Memory = self.borrow_mut();
                        memory.set_u16(current_address + SIBLING_OFFSET, sibling);
                        break;
                    }
                    current_address = self.get_object_address(next_child);
                    next_child = {
                        let memory: &Memory = self.borrow_mut();
                        ObjectId(memory.u16_at(current_address + SIBLING_OFFSET))
                    }
                }
            }
        }

        let memory: &mut Memory = self.borrow_mut();
        // Get the current destination child
        let previous_child = memory.u16_at(destination_address + CHILD_OFFSET);
        // Set the destination child to the object
        memory.set_uvalue(destination_address + CHILD_OFFSET, object);
        // Set the object sibling to the previous child of the destination
        memory.set_u16(object_address + SIBLING_OFFSET, previous_child);
        // Set the object parent to the destination
        memory.set_uvalue(object_address + PARENT_OFFSET, destination);
    }

    fn remove_obj(&mut self, object: ObjectId) {
        let start_of_object_table: &AbsoluteAddress = self.borrow_mut();

        let object_address = self.get_object_address(object);

        let sibling_id;
        let parent_id;
        {
            let memory: &mut Memory = self.borrow_mut();
            let object = MutableObjectView {
                memory,
                start_of_object: object_address
            };
            sibling_id = object.get_sibling();
            parent_id = object.get_parent();
        }

        let parent_address = { self.get_object_address(parent_id) };

        let memory: &mut Memory = self.borrow_mut();
        memory.set_uvalue(object_address + PARENT_OFFSET, NULL_OBJECT);
        memory.set_uvalue(parent_address + CHILD_OFFSET, sibling_id);
    }

    fn get_object_mut(&mut self, object: ObjectId) -> MutableObjectView {
        let object_address = { self.get_object_address(object) };
        let memory: &mut Memory = self.borrow_mut();
        MutableObjectView {
            memory,
            start_of_object: object_address
        }
    }
}

pub struct ObjectTableView<'a> {
    pub memory: &'a Memory,
    pub start_of_object_table: AbsoluteAddress
}
impl <'a> Borrow<Memory> for ObjectTableView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for ObjectTableView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_object_table
    }
}
impl <'a> AccessObjects for ObjectTableView<'a> {}
impl <'a> Debug for ObjectTableView<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let mut string_decoder = V5Decoder::new(DEFAULT_ALPHABET_V2);
        writeln!(f, "Objects:")?;
        let start_of_properties = self.get_object(ObjectId(1)).get_properties_address().to_address();
        let mut object_id = 1;
        let mut object_address = self.get_object_address(ObjectId(object_id));
        let mut attributes = Vec::new();
        while object_address < start_of_properties {
            writeln!(f, "    {}:", object_id)?;
            let object = self.get_object(ObjectId(object_id));
            for attribute in 0..31 {
                if object.get_attribute(attribute) {
                    attributes.push(attribute);
                }
            }
            writeln!(f, "        Attributes: {}", format!("{:?}", attributes))?;
            writeln!(f, "        Parent: {} Sibling: {} Child: {}", object.get_parent().0, object.get_sibling().0, object.get_child().0)?;
            writeln!(f, "        Properties table address: {:08X}", object.get_properties_address())?;
            let object_name = string_decoder
                .decode_string(self.memory, object.get_properties_table().get_name_address())
                .map_err(|e| Error)?;
            writeln!(f, "        Name: {}", object_name.0)?;
            writeln!(f, "        Properties:")?;
            for property_view in object.get_properties_table().iter() {
                let metadata: &PropertyMetadata = property_view.borrow();
                let data_address = metadata.header_addr + metadata.header_size;
                write!(f, "            {}: ", metadata.number)?;
                for index in 0..metadata.data_size {
                    write!(f, "{:02X}", self.memory.at(data_address + index))?;
                }
                writeln!(f)?;
            }

            attributes.clear();

            object_id += 1;
            object_address = self.get_object_address(ObjectId(object_id));
        };
        Ok(())
    }
}

pub struct MutableObjectTableView<'a> {
    pub memory: &'a mut Memory,
    pub start_of_object_table: AbsoluteAddress
}
impl <'a> Borrow<Memory> for MutableObjectTableView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> BorrowMut<Memory> for MutableObjectTableView<'a> {
    fn borrow_mut(&mut self) -> &mut Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for MutableObjectTableView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_object_table
    }
}
impl <'a> BorrowMut<AbsoluteAddress> for MutableObjectTableView<'a> {
    fn borrow_mut(&mut self) -> &mut AbsoluteAddress {
        &mut self.start_of_object_table
    }
}
impl <'a> AccessObjects for MutableObjectTableView<'a> {}
impl <'a> ModifyObjects for MutableObjectTableView<'a> {}

pub trait AccessObject : Borrow<Memory> + Borrow<AbsoluteAddress> {
    fn get_attribute(&self, attribute: u8) -> bool {
        let byte_index = attribute / 8;
        let bit_index = 7 - (attribute % 8);
        let byte_mask = 1 << bit_index;

        let memory: &Memory = self.borrow();
        let start_of_object: &AbsoluteAddress = self.borrow();

        byte_mask & memory.at(*start_of_object + byte_index) != 0
    }

    fn get_parent(&self) -> ObjectId {
        let memory: &Memory = self.borrow();
        let start_of_object: &AbsoluteAddress = self.borrow();

        ObjectId(memory.u16_at(*start_of_object + PARENT_OFFSET))
    }

    fn get_sibling(&self) -> ObjectId {
        let memory: &Memory = self.borrow();
        let start_of_object: &AbsoluteAddress = self.borrow();

        ObjectId(memory.u16_at(*start_of_object + SIBLING_OFFSET))
    }

    fn get_child(&self) -> ObjectId {
        let memory: &Memory = self.borrow();
        let start_of_object: &AbsoluteAddress = self.borrow();

        ObjectId(memory.u16_at(*start_of_object + CHILD_OFFSET))
    }

    fn get_properties_address(&self) -> ByteAddress {
        let memory: &Memory = self.borrow();
        let start_of_object: &AbsoluteAddress = self.borrow();

        ByteAddress(memory.u16_at(*start_of_object + PROPERTIES_OFFSET))
    }

    fn get_properties_table(&self) -> PropertyTableView {
        let address = self.get_properties_address();
        let memory: &Memory = self.borrow();
        PropertyTableView {
            memory: memory,
            start_of_property_table: address.to_address()
        }
    }
}

pub trait ModifyObject : AccessObject + BorrowMut<Memory> + BorrowMut<AbsoluteAddress> {
    fn set_attribute(&mut self, attribute: u8, flag: bool) {
        let byte_index = attribute / 8;
        let bit_index = 7 - (attribute % 8);
        let byte_mask = 1 << bit_index;

        let start_of_object: &AbsoluteAddress = self.borrow_mut();
        let address = *start_of_object + byte_index;

        let memory: &mut Memory = self.borrow_mut();
        let current_value = memory.at(address);
        let new_value = if flag {
            current_value | byte_mask
        } else {
            current_value & !byte_mask
        };
        memory.set(address, new_value);
    }

    fn set_parent(&mut self, parent: ObjectId) {
        let start_of_object: &AbsoluteAddress = self.borrow_mut();
        let address = *start_of_object + PARENT_OFFSET;
        let memory: &mut Memory = self.borrow_mut();
        memory.set_uvalue(address, parent);
    }

    fn set_sibling(&mut self, sibling: ObjectId) {
        let start_of_object: &AbsoluteAddress = self.borrow_mut();
        let address = *start_of_object + SIBLING_OFFSET;
        let memory: &mut Memory = self.borrow_mut();
        memory.set_uvalue(address, sibling);
    }

    fn set_child(&mut self, child: ObjectId) {
        let start_of_object: &AbsoluteAddress = self.borrow_mut();
        let address = *start_of_object + CHILD_OFFSET;
        let memory: &mut Memory = self.borrow_mut();
        memory.set_uvalue(address, child);
    }

    fn set_properties_address(&mut self, new_address: ByteAddress) {
        let start_of_object: &AbsoluteAddress = self.borrow_mut();
        let address = *start_of_object + PROPERTIES_OFFSET;
        let memory: &mut Memory = self.borrow_mut();
        memory.set_uvalue(address, new_address);
    }

    fn get_properties_table_mut(&mut self) -> MutablePropertyTableView {
        let address = self.get_properties_address();
        let memory: &mut Memory = self.borrow_mut();
        MutablePropertyTableView {
            memory,
            start_of_property_table: address.to_address()
        }
    }
}

pub struct PropertyIterator<'a> {
    memory: &'a Memory,
    offset: AbsoluteAddress
}
impl <'a> Iterator for PropertyIterator<'a> {
    type Item = PropertyView<'a>;

    fn next(&mut self) -> Option<PropertyView<'a>> {
        match get_property_metadata(self.memory, self.offset) {
            Some(metadata) => {
                self.offset += metadata.header_size + metadata.data_size;
                Some(PropertyView {
                    memory: self.memory,
                    metadata
                })
            },
            None => None
        }
    }
}

pub struct ObjectView<'a> {
    memory: &'a Memory,
    start_of_object: AbsoluteAddress
}
impl <'a> Borrow<Memory> for ObjectView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for ObjectView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_object
    }
}
impl <'a> AccessObject for ObjectView<'a> {}

pub struct MutableObjectView<'a> {
    memory: &'a mut Memory,
    start_of_object: AbsoluteAddress
}
impl <'a> Borrow<Memory> for MutableObjectView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> BorrowMut<Memory> for MutableObjectView<'a> {
    fn borrow_mut(&mut self) -> &mut Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for MutableObjectView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_object
    }
}
impl <'a> BorrowMut<AbsoluteAddress> for MutableObjectView<'a> {
    fn borrow_mut(&mut self) -> &mut AbsoluteAddress {
        &mut self.start_of_object
    }
}
impl <'a> AccessObject for MutableObjectView<'a> {}
impl <'a> ModifyObject for MutableObjectView<'a> {}

pub trait AccessProperties : Borrow<Memory> + Borrow<AbsoluteAddress> {
    fn get_name_length(&self) -> u16 {
        let memory: &Memory = self.borrow();
        let start_of_property_table: &AbsoluteAddress = self.borrow();
        memory.at(*start_of_property_table) as u16 * 2
    }

    fn get_name_address(&self) -> AbsoluteAddress {
        let start_of_property_table: &AbsoluteAddress = self.borrow();
        *start_of_property_table + 1
    }

    fn find_property_metadata(&self, property_number: u8) -> Option<PropertyMetadata> {
        let mut current_offset = 1 + self.get_name_length();
        loop {
            let start_of_property_table: &AbsoluteAddress = self.borrow();
            let header_addr = *start_of_property_table + current_offset;
            let memory: &Memory = self.borrow();
            match get_property_metadata(memory, header_addr) {
                Some(metadata) => {
                    if metadata.number == property_number {
                        return Some(metadata);
                    } else {
                        current_offset += metadata.header_size as u16 + metadata.data_size as u16;
                    }
                },
                None => {
                    return Option::None
                }
            }
        }
    }

    fn find_property_addr(&self, property_number: u8) -> Option<AbsoluteAddress> {
        self
            .find_property_metadata(property_number)
            .map(|metadata| metadata.header_addr + metadata.header_size)
    }

    fn find_property(&self, property_number: u8) -> Option<PropertyView> {
        self
            .find_property_metadata(property_number)
            .map(move |metadata| {
                let memory: &Memory = self.borrow();
                return PropertyView {
                    memory,
                    metadata
                };
            })
    }

    fn iter(&self) -> PropertyIterator {
        let memory: &Memory = self.borrow();
        let start_of_property_table: &AbsoluteAddress = self.borrow();
        PropertyIterator {
            memory,
            offset: *start_of_property_table + 1 + self.get_name_length()
        }
    }
}

pub trait ModifyProperties : AccessProperties + BorrowMut<Memory> {
    fn find_property_mut(&mut self, property_number: u8) -> Option<MutablePropertyView> {
        self
            .find_property_metadata(property_number)
            .map(move |metadata| {
                let memory: &mut Memory = self.borrow_mut();
                return MutablePropertyView {
                    memory,
                    metadata
                };
            })
    }
}

pub struct PropertyTableView<'a> {
    pub memory: &'a Memory,
    pub start_of_property_table: AbsoluteAddress
}
impl <'a> Borrow<Memory> for PropertyTableView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for PropertyTableView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_property_table
    }
}
impl <'a> AccessProperties for PropertyTableView<'a> {}

pub struct MutablePropertyTableView<'a> {
    pub memory: &'a mut Memory,
    pub start_of_property_table: AbsoluteAddress
}
impl <'a> Borrow<Memory> for MutablePropertyTableView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> BorrowMut<Memory> for MutablePropertyTableView<'a> {
    fn borrow_mut(&mut self) -> &mut Memory {
        self.memory
    }
}
impl <'a> Borrow<AbsoluteAddress> for MutablePropertyTableView<'a> {
    fn borrow(&self) -> &AbsoluteAddress {
        &self.start_of_property_table
    }
}
impl <'a> BorrowMut<AbsoluteAddress> for MutablePropertyTableView<'a> {
    fn borrow_mut(&mut self) -> &mut AbsoluteAddress {
        &mut self.start_of_property_table
    }
}
impl <'a> AccessProperties for MutablePropertyTableView<'a> {}
impl <'a> ModifyProperties for MutablePropertyTableView<'a> {}

pub trait AccessProperty: Borrow<Memory> + Borrow<PropertyMetadata> {
    /// Gets the value of the property.
    ///
    /// Only the first one or two bytes can be accessed this way.
    fn get_u16(&self) -> u16 {
        let metadata: &PropertyMetadata = self.borrow();
        let data_address = metadata.header_addr + metadata.header_size;

        let memory: &Memory = self.borrow();
        if metadata.data_size >= 2 {
            memory.u16_at(data_address)
        } else {
            memory.at(data_address) as u16
        }
    }
}

pub trait ModifyProperty: BorrowMut<Memory> + BorrowMut<PropertyMetadata> {
    /// Sets the value of the property.
    ///
    /// The value should be appropriate for property.
    /// If the property only has room for a single byte, the top half of the value is discarded.
    /// If the property more than two bytes the value is not set.
    fn set_u16(&mut self, value: u16) {
        let metadata: &PropertyMetadata = self.borrow_mut();
        let data_address = metadata.header_addr + metadata.header_size;

        if metadata.data_size == 1 {
            let memory: &mut Memory = self.borrow_mut();
            memory.set(data_address, (value & 0x0F) as u8);
        } else if metadata.data_size == 2 {
            let memory: &mut Memory = self.borrow_mut();
            memory.set_u16(data_address, value);
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct PropertyMetadata {
    header_addr: AbsoluteAddress,
    pub number: u8,
    header_size: u8,
    pub data_size: u8
}

pub struct PropertyView<'a> {
    memory: &'a Memory,
    metadata: PropertyMetadata
}
impl <'a> Borrow<Memory> for PropertyView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> Borrow<PropertyMetadata> for PropertyView<'a> {
    fn borrow(&self) -> &PropertyMetadata {
        &self.metadata
    }
}
impl <'a> AccessProperty for PropertyView<'a> {}

pub struct MutablePropertyView<'a> {
    memory: &'a mut Memory,
    metadata: PropertyMetadata
}
impl <'a> Borrow<Memory> for MutablePropertyView<'a> {
    fn borrow(&self) -> &Memory {
        self.memory
    }
}
impl <'a> BorrowMut<Memory> for MutablePropertyView<'a> {
    fn borrow_mut(&mut self) -> &mut Memory {
        self.memory
    }
}
impl <'a> Borrow<PropertyMetadata> for MutablePropertyView<'a> {
    fn borrow(&self) -> &PropertyMetadata {
        &self.metadata
    }
}
impl <'a> BorrowMut<PropertyMetadata> for MutablePropertyView<'a> {
    fn borrow_mut(&mut self) -> &mut PropertyMetadata {
        &mut self.metadata
    }
}
impl <'a> AccessProperty for MutablePropertyView<'a> {}
impl <'a> ModifyProperty for MutablePropertyView<'a> {}

fn get_property_metadata(memory: &Memory, header_addr: AbsoluteAddress) -> Option<PropertyMetadata> {
    let header_0 = memory.at(header_addr);
    if header_0 == 0 {
        return Option::None
    }

    let number = header_0 & 0b00111111;
    if header_0 & 0x80 != 0 {
        let header_1 = memory.at(header_addr + 1);
        let data_size = header_1 & 0b00111111;
        Some(PropertyMetadata {
            header_addr,
            number,
            header_size: 2,
            data_size
        })
    } else {
        let data_size = ((header_0 & 0b01000000) >> 6) + 1;
        Some(PropertyMetadata {
            header_addr,
            number,
            header_size: 1,
            data_size
        })
    }
}

/// Return the length of the property.
///
/// The address given is the address of the data. To get the length inspect the
/// previous address and determine the header size.
pub fn get_prop_len(memory: &Memory, data_address: AbsoluteAddress) -> u8 {
    let header_fragment = memory.at(data_address - 1);
    if header_fragment & 0x80 != 0 {
        // Header size must be 2
        header_fragment & 0b00111111
    } else {
        // Header size must be 1
        ((header_fragment & 0b01000000) >> 6) + 1
    }
}

mod tests {
    use std::fs::File;
    use common::memory::Memory;
    use v5::header::parse_header;
    use v5::objects::{ObjectTableView, AccessObjects, ObjectId, AccessObject, NULL_OBJECT, ObjectView, AccessProperties, AccessProperty, MutableObjectTableView, ModifyObjects};
    use common::address::ToAbsoluteAddress;
    use common::address::AbsoluteAddress;

    #[test]
    fn test_parents() {
        let memory = get_memory();
        let object_table = get_object_table(&memory);
        let objects = get_objects(&object_table);

        assert_eq!(NULL_OBJECT, objects[0].get_parent());
        assert_eq!(NULL_OBJECT, objects[1].get_parent());
        assert_eq!(NULL_OBJECT, objects[2].get_parent());
        assert_eq!(NULL_OBJECT, objects[3].get_parent());
        assert_eq!(ObjectId(1), objects[4].get_parent());
        assert_eq!(NULL_OBJECT, objects[5].get_parent());
        assert_eq!(ObjectId(6), objects[6].get_parent());
        assert_eq!(ObjectId(6), objects[7].get_parent());
        assert_eq!(ObjectId(6), objects[8].get_parent());
        assert_eq!(ObjectId(6), objects[9].get_parent());
        assert_eq!(ObjectId(6), objects[10].get_parent());
        assert_eq!(ObjectId(6), objects[11].get_parent());
        assert_eq!(ObjectId(6), objects[12].get_parent());
        assert_eq!(ObjectId(6), objects[13].get_parent());
        assert_eq!(ObjectId(6), objects[14].get_parent());
        assert_eq!(ObjectId(6), objects[15].get_parent());
        assert_eq!(ObjectId(6), objects[16].get_parent());
        assert_eq!(ObjectId(6), objects[17].get_parent());
        assert_eq!(NULL_OBJECT, objects[18].get_parent());
        assert_eq!(ObjectId(1), objects[19].get_parent());
        assert_eq!(NULL_OBJECT, objects[20].get_parent());
        assert_eq!(NULL_OBJECT, objects[21].get_parent());
        assert_eq!(NULL_OBJECT, objects[22].get_parent());
        assert_eq!(NULL_OBJECT, objects[23].get_parent());
        assert_eq!(NULL_OBJECT, objects[24].get_parent());
        assert_eq!(NULL_OBJECT, objects[25].get_parent());
        assert_eq!(NULL_OBJECT, objects[26].get_parent());
        assert_eq!(NULL_OBJECT, objects[27].get_parent());
        assert_eq!(NULL_OBJECT, objects[28].get_parent());
        assert_eq!(NULL_OBJECT, objects[29].get_parent());
    }

    #[test]
    fn test_siblings() {
        let memory = get_memory();
        let object_table = get_object_table(&memory);
        let objects = get_objects(&object_table);

        assert_eq!(NULL_OBJECT, objects[0].get_sibling());
        assert_eq!(NULL_OBJECT, objects[1].get_sibling());
        assert_eq!(NULL_OBJECT, objects[2].get_sibling());
        assert_eq!(NULL_OBJECT, objects[3].get_sibling());
        assert_eq!(ObjectId(20), objects[4].get_sibling());
        assert_eq!(NULL_OBJECT, objects[5].get_sibling());
        assert_eq!(ObjectId(8), objects[6].get_sibling());
        assert_eq!(ObjectId(9), objects[7].get_sibling());
        assert_eq!(ObjectId(10), objects[8].get_sibling());
        assert_eq!(ObjectId(11), objects[9].get_sibling());
        assert_eq!(ObjectId(12), objects[10].get_sibling());
        assert_eq!(ObjectId(13), objects[11].get_sibling());
        assert_eq!(ObjectId(14), objects[12].get_sibling());
        assert_eq!(ObjectId(15), objects[13].get_sibling());
        assert_eq!(ObjectId(16), objects[14].get_sibling());
        assert_eq!(ObjectId(17), objects[15].get_sibling());
        assert_eq!(ObjectId(18), objects[16].get_sibling());
        assert_eq!(NULL_OBJECT, objects[17].get_sibling());
        assert_eq!(NULL_OBJECT, objects[18].get_sibling());
        assert_eq!(NULL_OBJECT, objects[19].get_sibling());
        assert_eq!(NULL_OBJECT, objects[20].get_sibling());
        assert_eq!(NULL_OBJECT, objects[21].get_sibling());
        assert_eq!(NULL_OBJECT, objects[22].get_sibling());
        assert_eq!(NULL_OBJECT, objects[23].get_sibling());
        assert_eq!(NULL_OBJECT, objects[24].get_sibling());
        assert_eq!(NULL_OBJECT, objects[25].get_sibling());
        assert_eq!(NULL_OBJECT, objects[26].get_sibling());
        assert_eq!(NULL_OBJECT, objects[27].get_sibling());
        assert_eq!(NULL_OBJECT, objects[28].get_sibling());
        assert_eq!(NULL_OBJECT, objects[29].get_sibling());
    }

    #[test]
    fn test_children() {
        let memory = get_memory();
        let object_table = get_object_table(&memory);
        let objects = get_objects(&object_table);

        assert_eq!(ObjectId(5), objects[0].get_child());
        assert_eq!(NULL_OBJECT, objects[1].get_child());
        assert_eq!(NULL_OBJECT, objects[2].get_child());
        assert_eq!(NULL_OBJECT, objects[3].get_child());
        assert_eq!(NULL_OBJECT, objects[4].get_child());
        assert_eq!(ObjectId(7), objects[5].get_child());
        assert_eq!(NULL_OBJECT, objects[6].get_child());
        assert_eq!(NULL_OBJECT, objects[7].get_child());
        assert_eq!(NULL_OBJECT, objects[8].get_child());
        assert_eq!(NULL_OBJECT, objects[9].get_child());
        assert_eq!(NULL_OBJECT, objects[10].get_child());
        assert_eq!(NULL_OBJECT, objects[11].get_child());
        assert_eq!(NULL_OBJECT, objects[12].get_child());
        assert_eq!(NULL_OBJECT, objects[13].get_child());
        assert_eq!(NULL_OBJECT, objects[14].get_child());
        assert_eq!(NULL_OBJECT, objects[15].get_child());
        assert_eq!(NULL_OBJECT, objects[16].get_child());
        assert_eq!(NULL_OBJECT, objects[17].get_child());
        assert_eq!(NULL_OBJECT, objects[18].get_child());
        assert_eq!(NULL_OBJECT, objects[19].get_child());
        assert_eq!(NULL_OBJECT, objects[20].get_child());
        assert_eq!(NULL_OBJECT, objects[21].get_child());
        assert_eq!(NULL_OBJECT, objects[22].get_child());
        assert_eq!(NULL_OBJECT, objects[23].get_child());
        assert_eq!(NULL_OBJECT, objects[24].get_child());
        assert_eq!(NULL_OBJECT, objects[25].get_child());
        assert_eq!(NULL_OBJECT, objects[26].get_child());
        assert_eq!(NULL_OBJECT, objects[27].get_child());
        assert_eq!(NULL_OBJECT, objects[28].get_child());
        assert_eq!(NULL_OBJECT, objects[29].get_child());
    }

    #[test]
    fn test_attributes() {
        let memory = get_memory();
        let object_table = get_object_table(&memory);
        let objects = get_objects(&object_table);

        assert!(!objects[0].get_attribute(0));
        assert!(!objects[0].get_attribute(1));
        assert!(!objects[0].get_attribute(2));
        assert!(!objects[1].get_attribute(0));
        assert!(!objects[1].get_attribute(1));
        assert!(!objects[1].get_attribute(2));
        assert!(!objects[5].get_attribute(1));
        assert!(!objects[5].get_attribute(2));
        assert!(objects[5].get_attribute(3));
        assert!(!objects[6].get_attribute(16));
        assert!(objects[6].get_attribute(17));
        assert!(!objects[6].get_attribute(18));
    }

    #[test]
    fn test_properties() {
        let memory = get_memory();
        let object_table = get_object_table(&memory);
        let objects = get_objects(&object_table);

        assert_eq!(AbsoluteAddress(0x032C), objects[0].get_properties_address().to_address());
        assert_eq!(AbsoluteAddress(0x033C), objects[1].get_properties_address().to_address());
        assert_eq!(AbsoluteAddress(0x034E), objects[2].get_properties_address().to_address());
        assert_eq!(AbsoluteAddress(0x06BC), objects[28].get_properties_address().to_address());
        assert_eq!(AbsoluteAddress(0x06C9), objects[29].get_properties_address().to_address());

        let properties_table = objects[6].get_properties_table();
        let property_48 = properties_table.find_property(48).unwrap();
        assert_eq!(2, property_48.metadata.data_size);
        assert_eq!(0x09ED, property_48.get_u16());

        let property_1 = properties_table.find_property(1).unwrap();
        assert_eq!(4, property_1.metadata.data_size);
        let property_address = property_1.metadata.header_addr + property_1.metadata.header_size;

        assert_eq!(0x21, memory.at(property_address));
        assert_eq!(0x81, memory.at(property_address + 1));
        assert_eq!(0x21, memory.at(property_address + 2));
        assert_eq!(0xC0, memory.at(property_address + 3));
    }

    #[test]
    fn test_insert_object() {
        let mut memory = get_memory();
        let mut object_table = get_mutable_object_table(&mut memory);

        object_table.insert_obj(ObjectId(28), ObjectId(20));
        // New parent
        {
            let object = object_table.get_object_mut(ObjectId(20));
            assert_eq!(ObjectId(1), object.get_parent());
            assert_eq!(ObjectId(28), object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // Object moved
        {
            let object = object_table.get_object_mut(ObjectId(28));
            assert_eq!(ObjectId(20), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }

        object_table.insert_obj(ObjectId(29), ObjectId(20));
        // New parent
        {
            let object = object_table.get_object_mut(ObjectId(20));
            assert_eq!(ObjectId(1), object.get_parent());
            assert_eq!(ObjectId(29), object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // New sibling
        {
            let object = object_table.get_object_mut(ObjectId(28));
            assert_eq!(ObjectId(20), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // Object moved
        {
            let object = object_table.get_object_mut(ObjectId(29));
            assert_eq!(ObjectId(20), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
            assert_eq!(ObjectId(28), object.get_sibling());
        }

        object_table.insert_obj(ObjectId(28), ObjectId(27));
        // Source
        {
            let object = object_table.get_object_mut(ObjectId(20));
            assert_eq!(ObjectId(1), object.get_parent());
            assert_eq!(ObjectId(29), object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // Object being moved
        {
            let object = object_table.get_object_mut(ObjectId(28));
            assert_eq!(ObjectId(27), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // Old sibling
        {
            let object = object_table.get_object_mut(ObjectId(29));
            assert_eq!(ObjectId(20), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
            assert_eq!(NULL_OBJECT, object.get_sibling());
        }
        // New parent
        {
            let player = object_table.get_object_mut(ObjectId(27));
            assert_eq!(NULL_OBJECT, player.get_parent());
            assert_eq!(ObjectId(28), player.get_child());
            assert_eq!(NULL_OBJECT, player.get_sibling());
        }
    }

    #[test]
    fn test_remove_object() {
        let mut memory = get_memory();
        let mut object_table = get_mutable_object_table(&mut memory);

        object_table.insert_obj(ObjectId(28), ObjectId(20));
        object_table.insert_obj(ObjectId(27), ObjectId(28));
        object_table.insert_obj(ObjectId(26), ObjectId(27));
        object_table.insert_obj(ObjectId(25), ObjectId(26));

        object_table.remove_obj(ObjectId(25));
        // Removed
        {
            let object = object_table.get_object_mut(ObjectId(25));
            assert_eq!(NULL_OBJECT, object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
        }
        // Removed's parent
        {
            let object = object_table.get_object_mut(ObjectId(26));
            assert_eq!(ObjectId(27), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
        }

        object_table.remove_obj(ObjectId(27));
        // Removed's child
        {
            let object = object_table.get_object_mut(ObjectId(26));
            assert_eq!(ObjectId(27), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
        }
        // Removed
        {
            let object = object_table.get_object_mut(ObjectId(27));
            assert_eq!(NULL_OBJECT, object.get_parent());
            assert_eq!(ObjectId(26), object.get_child());
        }
        // Removed's parent
        {
            let object = object_table.get_object_mut(ObjectId(28));
            assert_eq!(ObjectId(20), object.get_parent());
            assert_eq!(NULL_OBJECT, object.get_child());
        }
    }

    fn get_memory() -> Memory {
        let mut file = File::open("samples/bin/a-locked-room.z5").unwrap();
        Memory::initialise_memory(&mut file).unwrap()
    }

    fn get_object_table(memory: &Memory) -> ObjectTableView {
        let header = parse_header(&memory);
        ObjectTableView {
            memory,
            start_of_object_table: header.object_table.to_address()
        }
    }

    fn get_mutable_object_table(memory: &mut Memory) -> MutableObjectTableView {
        let header = parse_header(&memory);
        MutableObjectTableView {
            memory,
            start_of_object_table: header.object_table.to_address()
        }
    }

    fn get_objects<'a>(object_table: &'a ObjectTableView) -> [ObjectView<'a>; 30] {
        [
            object_table.get_object(ObjectId(1)),
            object_table.get_object(ObjectId(2)),
            object_table.get_object(ObjectId(3)),
            object_table.get_object(ObjectId(4)),
            object_table.get_object(ObjectId(5)),
            object_table.get_object(ObjectId(6)),
            object_table.get_object(ObjectId(7)),
            object_table.get_object(ObjectId(8)),
            object_table.get_object(ObjectId(9)),
            object_table.get_object(ObjectId(10)),
            object_table.get_object(ObjectId(11)),
            object_table.get_object(ObjectId(12)),
            object_table.get_object(ObjectId(13)),
            object_table.get_object(ObjectId(14)),
            object_table.get_object(ObjectId(15)),
            object_table.get_object(ObjectId(16)),
            object_table.get_object(ObjectId(17)),
            object_table.get_object(ObjectId(18)),
            object_table.get_object(ObjectId(19)),
            object_table.get_object(ObjectId(20)),
            object_table.get_object(ObjectId(21)),
            object_table.get_object(ObjectId(22)),
            object_table.get_object(ObjectId(23)),
            object_table.get_object(ObjectId(24)),
            object_table.get_object(ObjectId(25)),
            object_table.get_object(ObjectId(26)),
            object_table.get_object(ObjectId(27)),
            object_table.get_object(ObjectId(28)),
            object_table.get_object(ObjectId(29)),
            object_table.get_object(ObjectId(30))
        ]
    }
}
