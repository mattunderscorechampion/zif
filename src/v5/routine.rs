
use common::address::AbsoluteAddress;
use common::address::ToAbsoluteAddress;
use common::memory::Memory;
use v5::address::PackedAddress;
use std::io::{Result, Error, ErrorKind};

#[derive(Debug, Copy, Clone)]
pub struct Routine {
    pub number_of_local_variables: u8,
    pub body: AbsoluteAddress
}

pub fn decode_routine(memory: &Memory, address: PackedAddress) -> Result<Routine> {
    let addr = address.to_address();
    let number_of_local_variables = memory.at(addr);
    if number_of_local_variables > 15 {
        Err(Error::new(ErrorKind::Other, format!("Invalid number of local variables: {:08X} has {}", addr, number_of_local_variables)))?
    } else {
        Ok(Routine {
            number_of_local_variables,
            body: addr + 1
        })
    }
}
