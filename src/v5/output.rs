use common::output::OutputStream;
use std::io::Error;
use std::io::Write;

pub struct OutputStreams {
    screen: OutputStream,
    transcript: OutputStream
}
impl Write for OutputStreams {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        self.screen.write_all(buf)?;
        self.transcript.write_all(buf)?;
        Result::Ok(buf.len())
    }

    fn flush(&mut self) -> Result<(), Error> {
        self.screen.flush()?;
        self.transcript.flush()
    }
}

pub struct Output {
    streams: OutputStreams,
    debug: OutputStream,
    error: OutputStream
}
impl Output {
    pub fn new() -> Output {
        Output {
            streams: OutputStreams {
                screen: OutputStream::Stdout,
                transcript: OutputStream::Sink
            },
            debug: OutputStream::Sink,
            error: OutputStream::Stderr
        }
    }

    pub fn with_debug(self, debug: OutputStream) -> Output {
        Output {
            streams: self.streams,
            debug,
            error: self.error
        }
    }

    pub fn with_transcript(self, transcript: OutputStream) -> Output {
        Output {
            streams: OutputStreams {
                screen: self.streams.screen,
                transcript
            },
            debug: self.debug,
            error: self.error
        }
    }

    pub fn debug(&mut self) -> &mut OutputStream {
        &mut self.debug
    }

    pub fn error(&mut self) -> &mut OutputStream {
        &mut self.error
    }

    pub fn streams(&mut self) -> &mut OutputStreams {
        &mut self.streams
    }
}
