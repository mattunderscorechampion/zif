use std::fmt;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Formatter;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Result;

use common::address::AbsoluteAddress;
use common::branch::BranchArgument;
use common::instruction::decode_extended_form_instruction;
use common::instruction::decode_long_form_instruction;
use common::instruction::decode_short_form_instruction;
use common::instruction::decode_variable_2op_instruction;
use common::instruction::decode_variable_var_op_instruction;
use common::instruction::Form;
use common::memory::Memory;
use common::operand::Operand;
use v5::z_char::V5Decoder;
use v5::address::PackedAddress;
use std::mem::transmute;

fn format_operand(f: &mut Formatter, operand: &Operand) -> fmt::Result<> {
    match operand {
        Operand::SmallConstant(c) => f.write_fmt(format_args!(" {:02X?}", c)),
        Operand::LargeConstant(c) => f.write_fmt(format_args!(" {:04X?}", c)),
        Operand::Variable(v) => {
            if *v == 0 {
                f.write_fmt(format_args!(" SP"))
            } else if *v < 16 {
                f.write_fmt(format_args!(" L{:02X}", *v - 1))
            } else {
                f.write_fmt(format_args!(" G{:02X}", *v - 16))
            }
        }
    }
}

fn format_operand_as_packed_address(f: &mut Formatter, operand: &Operand) -> fmt::Result<> {
    match operand {
        Operand::SmallConstant(c) => {
            f.write_fmt(format_args!(" {:08X}", PackedAddress(*c as u16)))
        },
        Operand::LargeConstant(c) => {
            f.write_fmt(format_args!(" {:08X}", PackedAddress(*c)))
        },
        Operand::Variable(v) => {
            if *v == 0 {
                f.write_fmt(format_args!(" SP"))
            } else if *v < 16 {
                f.write_fmt(format_args!(" L{:02X}", *v - 1))
            } else {
                f.write_fmt(format_args!(" G{:02X}", *v - 16))
            }
        }
    }
}

#[derive(PartialEq, Eq)]
pub struct Instruction {
    pub form: Form,
    pub opcode: u8,
    pub operands: Vec<Operand>,
    pub branch: Option<BranchArgument>,
    pub store: Option<u8>,
    pub text: Option<String>,
    pub size: usize
}
impl Display for Instruction {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result<> {
        match (self.opcode, self.form) {
            (0, Form::Extended) => f.write_str("SAVE"),
            (1, Form::Extended) => f.write_str("RESTORE"),
            (9, Form::Extended) => f.write_str("SAVE_UNDO"),
            (10, Form::Extended) => f.write_str("RESTORE_UNDO"),
            (_, Form::Extended) => f.write_fmt(format_args!("UNKNOWN extended form opcode {}", self.opcode)),
            (1, _) => f.write_str("JE"),
            (2, _) => f.write_str("JL"),
            (3, _) => f.write_str("JG"),
            (6, _) => f.write_str("JIN"),
            (8, _) => f.write_str("OR"),
            (9, _) => f.write_str("AND"),
            (10, _) => f.write_str("TEST_ATTR"),
            (11, _) => f.write_str("SET_ATTR"),
            (12, _) => f.write_str("CLEAR_ATTR"),
            (13, _) => f.write_str("STORE"),
            (14, _) => f.write_str("INSERT_OBJ"),
            (15, _) => f.write_str("LOADW"),
            (16, _) => f.write_str("LOADB"),
            (17, _) => f.write_str("GET_PROP"),
            (18, _) => f.write_str("GET_PROP_ADDR"),
            (20, _) => f.write_str("ADD"),
            (21, _) => f.write_str("SUB"),
            (22, _) => f.write_str("MUL"),
            (23, _) => f.write_str("DIV"),
            (24, _) => f.write_str("MOD_REMAINDER"),
            (25, _) => f.write_str("CALL_2S"),
            (26, _) => f.write_str("CALL_2N"),
            (128, _) => f.write_str("JZ"),
            (129, _) => f.write_str("GET_SIBLING"),
            (130, _) => f.write_str("GET_CHILD"),
            (133, _) => f.write_str("INC"),
            (134, _) => f.write_str("DEC"),
            (135, _) => f.write_str("PRINT_ADDR"),
            (136, _) => f.write_str("CALL_1S"),
            (137, _) => f.write_str("REMOVE_OBJ"),
            (138, _) => f.write_str("PRINT_OBJ"),
            (139, _) => f.write_str("RET"),
            (140, _) => f.write_str("JUMP"),
            (141, _) => f.write_str("PRINT_PADDR"),
            (143, _) => f.write_str("CALL_1N"),
            (144, _) => f.write_str("JZ"),
            (145, _) => f.write_str("GET_SIBLING"),
            (146, _) => f.write_str("GET_CHILD"),
            (149, _) => f.write_str("INC"),
            (150, _) => f.write_str("DEC"),
            (155, _) => f.write_str("RET"),
            (160, _) => f.write_str("JZ"),
            (161, _) => f.write_str("GET_SIBLING"),
            (162, _) => f.write_str("GET_CHILD"),
            (163, _) => f.write_str("GET_PARENT"),
            (164, _) => f.write_str("GET_PROP_LEN"),
            (167, _) => f.write_str("PRINT_ADDR"),
            (168, _) => f.write_str("CALL_1S"),
            (169, _) => f.write_str("REMOVE_OBJ"),
            (170, _) => f.write_str("PRINT_OBJ"),
            (171, _) => f.write_str("RET"),
            (173, _) => f.write_str("PRINT_PADDR"),
            (175, _) => f.write_str("CALL_1N"),
            (176, _) => f.write_str("RTRUE"),
            (177, _) => f.write_str("RFALSE"),
            (178, _) => f.write_str("PRINT"),
            (179, _) => f.write_str("PRINT_RET"),
            (184, _) => f.write_str("RET_PROPPED"),
            (186, _) => f.write_str("QUIT"),
            (187, _) => f.write_str("NEW_LINE"),
            (189, _) => f.write_str("VERIFY"),
            (224, _) => f.write_str("CALL_VS"),
            (225, _) => f.write_str("STOREW"),
            (226, _) => f.write_str("STOREB"),
            (227, _) => f.write_str("PUT_PROP"),
            (228, _) => f.write_str("READ"),
            (229, _) => f.write_str("PRINT_CHAR"),
            (230, _) => f.write_str("PRINT_NUM"),
            (231, _) => f.write_str("RANDOM"),
            (232, _) => f.write_str("PUSH"),
            (233, _) => f.write_str("PULL"),
            (234, _) => f.write_str("SPLIT_WINDOW"),
            (235, _) => f.write_str("SPLIT_WINDOW"),
            (236, _) => f.write_str("CALL_VS2"),
            (239, _) => f.write_str("SET_CURSOR"),
            (241, _) => f.write_str("SET_TEXT_STYLE"),
            (246, _) => f.write_str("READ_CHAR"),
            (248, _) => f.write_str("NOT"),
            (249, _) => f.write_str("CALL_VN"),
            (250, _) => f.write_str("CALL_VN2"),
            (251, _) => f.write_str("TOKENISE"),
            (255, _) => f.write_str("CHECK_ARG_COUNT"),
            _ => f.write_fmt(format_args!("UNKNOWN opcode {}", self.opcode))
        }?;
        if is_call(&self) {
            for (index, operand) in self.operands.iter().enumerate() {
                if index == 0 {
                    format_operand_as_packed_address(f, operand)?
                }
                else {
                    format_operand(f, operand)?;
                }
            }
        }
        else if is_jump(&self) {
            match self.operands[0] {
                Operand::SmallConstant(c) => f.write_fmt(format_args!(" {:02X?}", c - 2)),
                Operand::LargeConstant(c) => {
                    let value = unsafe { transmute::<u16, i16>(c) };
                    f.write_fmt(format_args!(" {:04X?}", value - 2))
                },
                Operand::Variable(v) => {
                    if v == 0 {
                        f.write_fmt(format_args!(" SP - 2"))
                    } else if v < 16 {
                        f.write_fmt(format_args!(" L{:02X} - 2", v - 1))
                    } else {
                        f.write_fmt(format_args!(" G{:02X} - 2", v - 16))
                    }
                }
            }?;
        }
        else {
            for operand in &self.operands {
                format_operand(f, operand)?;
            }
        }
        match &self.branch {
            Some(branch_argument) => {
                match branch_argument {
                    BranchArgument::Jump {
                        reverse_logic,
                        resume_address: _,
                        target_address
                    } => {
                        if *reverse_logic { f.write_str(" reverse")?; };
                        f.write_fmt(format_args!(" jump to {:08X}", target_address))?;
                    },
                    BranchArgument::ReturnFalse {
                        reverse_logic,
                        resume_address: _
                    } => {
                        if *reverse_logic { f.write_str(" reverse")?; };
                        f.write_str(" return false")?;
                    },
                    BranchArgument::ReturnTrue {
                        reverse_logic,
                        resume_address: _
                    } => {
                        if *reverse_logic { f.write_str(" reverse")?; };
                        f.write_str(" return true")?;
                    }
                }
            }
            None => {}
        };
        match &self.store {
            Some(v) => {
                if *v == 0 {
                    f.write_fmt(format_args!(" -> SP"))?;
                } else if *v < 16 {
                    f.write_fmt(format_args!(" -> L{:02X}", *v - 1))?;
                } else {
                    f.write_fmt(format_args!(" -> G{:02X}", *v - 16))?;
                }
            }
            None => {}
        };
        match &self.text {
            Some(text) => {
                f.write_fmt(format_args!(" {:?}", text))?;
            }
            None => {}
        };
        Ok(())
    }
}
impl Debug for Instruction {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result<> {
        f.write_str("Instruction { opcode: ")?;
        f.write_fmt(format_args!("{:03}", self.opcode))?;

        f.write_str(", form: ")?;
        match self.form {
            Form::Extended => {
                f.write_str("extended")?;
            },
            Form::Long => {
                f.write_str("long")?;
            },
            Form::Short => {
                f.write_str("short")?;
            },
            Form::Variable => {
                f.write_str("variable")?;
            }
        };

        f.write_str(", operands: [")?;
        let mut iter = self.operands.iter();
        match iter.next() {
            Some(operand) => {
                match operand {
                    Operand::SmallConstant(c) => f.write_fmt(format_args!("{:02X?}", c))?,
                    Operand::LargeConstant(c) => f.write_fmt(format_args!("{:04X?}", c))?,
                    Operand::Variable(v) => {
                        if *v == 0 {
                            f.write_str("SP")?;
                        } else if *v < 16 {
                            f.write_fmt(format_args!("L{:02X}", *v - 1))?;
                        } else {
                            f.write_fmt(format_args!("G{:02X}", *v - 16))?;
                        }
                    }
                }
            },
            None => {}
        }
        loop {
            match iter.next() {
                Some(operand) => {
                    match operand {
                        Operand::SmallConstant(c) => f.write_fmt(format_args!(", {:02X?}", c))?,
                        Operand::LargeConstant(c) => f.write_fmt(format_args!(", {:04X?}", c))?,
                        Operand::Variable(v) => {
                            if *v == 0 {
                                f.write_str(", SP")?;
                            } else if *v < 16 {
                                f.write_fmt(format_args!(", L{:02X}", *v - 1))?;
                            } else {
                                f.write_fmt(format_args!(", G{:02X}", *v - 16))?;
                            }
                        }
                    }
                },
                None => {
                    break;
                }
            }
        }
        f.write_str("]")?;

        match &self.branch {
            Some(branch_argument) => {
                f.write_str(", branch:")?;
                match branch_argument {
                    BranchArgument::Jump {
                        reverse_logic,
                        target_address,
                        resume_address
                    } => {
                        if *reverse_logic {
                            f.write_str(" reverse logic")?;
                        }
                        f.write_str(" jump to ")?;
                        f.write_fmt(format_args!("{:08X}", target_address))?;
                        f.write_str(" else ")?;
                        f.write_fmt(format_args!("{:08X}", resume_address))?;
                    },
                    BranchArgument::ReturnFalse {
                        reverse_logic,
                        resume_address
                    } => {
                        if *reverse_logic {
                            f.write_str(" reverse logic")?;
                        }
                        f.write_str(" ret false else ")?;
                        f.write_fmt(format_args!("{:08X}", resume_address))?;
                    },
                    BranchArgument::ReturnTrue {
                        reverse_logic,
                        resume_address
                    } => {
                        if *reverse_logic {
                            f.write_str(" reverse logic")?;
                        }
                        f.write_str(" ret true else ")?;
                        f.write_fmt(format_args!("{:08X}", resume_address))?;
                    }
                }
            },
            None => {}
        };

        match self.store {
            Some(v) => {
                f.write_str(", store: ")?;
                if v == 0 {
                    f.write_str("SP")?;
                } else if v < 16 {
                    f.write_fmt(format_args!("L{:02X}", v - 1))?;
                } else {
                    f.write_fmt(format_args!("G{:02X}", v - 16))?;
                }
            },
            None => {}
        };

        match &self.text {
            Some(text) => {
                f.write_str(", text: ")?;
                f.write_fmt(format_args!("{:?}", text))?;
            },
            None => {}
        };

        f.write_str( " }")
    }
}

pub fn is_call(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (25, _) => true,
        (26, _) => true,
        (136, _) => true,
        (143, _) => true,
        (168, _) => true,
        (224, _) => true,
        (236, _) => true,
        (249, _) => true,
        (250, _) => true,
        _ => false
    }
}

pub fn is_return(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (139, _) => true,
        (155, _) => true,
        (171, _) => true,
        (176, _) => true,
        (177, _) => true,
        (179, _) => true,
        (184, _) => true,
        (186, _) => true,
        _ => false
    }
}

pub fn is_store(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (0, Form::Extended) => true,
        (1, Form::Extended) => true,
        (9, Form::Extended) => true,
        (10, Form::Extended) => true,
        (_, Form::Extended) => false,
        (8, _) => true,
        (9, _) => true,
        (15, _) => true,
        (16, _) => true,
        (17, _) => true,
        (18, _) => true,
        (20, _) => true,
        (21, _) => true,
        (22, _) => true,
        (23, _) => true,
        (24, _) => true,
        (25, _) => true,
        (129, _) => true,
        (130, _) => true,
        (136, _) => true,
        (145, _) => true,
        (146, _) => true,
        (161, _) => true,
        (162, _) => true,
        (163, _) => true,
        (164, _) => true,
        (168, _) => true,
        (224, _) => true,
        (228, _) => true,
        (231, _) => true,
        (236, _) => true,
        (246, _) => true,
        (248, _) => true,
        _ => false
    }
}

pub fn is_branch(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (1, _) => true,
        (2, _) => true,
        (3, _) => true,
        (6, _) => true,
        (10, _) => true,
        (128, _) => true,
        (129, _) => true,
        (130, _) => true,
        (144, _) => true,
        (145, _) => true,
        (146, _) => true,
        (160, _) => true,
        (161, _) => true,
        (162, _) => true,
        (189, _) => true,
        (255, _) => true,
        _ => false
    }
}

pub fn has_text(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (178, _) => true,
        (179, _) => true,
        _ => false
    }
}

pub fn is_jump(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (140, _) => true,
        _ => false
    }
}

pub fn is_quit(instruction: &Instruction) -> bool {
    match (instruction.opcode, instruction.form) {
        (_, Form::Extended) => false,
        (186, _) => true,
        _ => false
    }
}

pub fn decode_instruction(memory: &Memory, string_decoder: &mut V5Decoder, position: AbsoluteAddress) -> Result<Instruction> {
    let opcode = memory.at(position);

    let common_instruction = if opcode < 0x80 {
        decode_long_form_instruction(memory, position)
    } else if opcode == 0xBE {
        decode_extended_form_instruction(memory, position)
    } else if opcode >= 0x80 && opcode < 0xBF {
        decode_short_form_instruction(memory, position)
    } else if opcode >= 0xC0 && opcode < 0xE0 {
        decode_variable_2op_instruction(memory, position)
    } else if opcode >= 0xE0 {
        decode_variable_var_op_instruction(memory, position)
    } else {
        return Err(Error::new(ErrorKind::Other, format!("Unknown opcode {:X}", opcode)))
    };

    let mut instruction = Instruction {
        opcode: common_instruction.opcode,
        form: common_instruction.form,
        size: common_instruction.size,
        operands: common_instruction.operands,
        branch: Option::None,
        store: Option::None,
        text: Option::None,
    };

    if is_store(&instruction) {
        instruction.store = Option::Some(memory.at(position + instruction.size));
        instruction.size += 1;
    }

    if is_branch(&instruction) {
        let (branch_argument, size) = BranchArgument::decode(memory, position + instruction.size);
        instruction.branch = Option::Some(branch_argument);
        instruction.size += size;
    }

    if has_text(&instruction) {
        let (string, bytes_read) = string_decoder.decode_string(&memory, position + instruction.size)?;
        instruction.text = Option::Some(string);
        instruction.size += bytes_read;
    }

    Ok(instruction)
}
