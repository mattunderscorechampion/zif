use common::address::AbsoluteAddress;
use common::address::ByteAddress;
use common::constant::header::ABBREVIATIONS;
use common::constant::header::BACKGROUND;
use common::constant::header::CHECKSUM;
use common::constant::header::COLUMNS;
use common::constant::header::DICTIONARY;
use common::constant::header::FILE_LENGTH;
use common::constant::header::FLAGS_1;
use common::constant::header::FLAGS_2;
use common::constant::header::FONT_HEIGHT;
use common::constant::header::FONT_WIDTH;
use common::constant::header::FOREGROUND;
use common::constant::header::GLOBAL_VARIABLES;
use common::constant::header::HEIGHT;
use common::constant::header::HIGH_MEMORY;
use common::constant::header::INITIAL_PROGRAM_COUNTER;
use common::constant::header::INTERPRETER_NUMBER;
use common::constant::header::INTERPRETER_VERSION;
use common::constant::header::LINES;
use common::constant::header::OBJECT_TABLE;
use common::constant::header::REVISION_NUMBER;
use common::constant::header::STATIC_MEMORY;
use common::constant::header::VERSION;
use common::constant::header::WIDTH;
use common::memory::Memory;

pub enum Flag {
    ColoursAvailable,
    BoldfaceAvailable,
    ItalicAvailable,
    FixedSpaceAvailable,
    TimedKeyboardInputAvailable,
    TranscriptRunning,
    ForceFixedPitchFont,
    UsePictures,
    UseUndoOpcodes,
    UseMouse,
    UseColours,
    UseSoundEffects
}
impl Flag {
    pub fn byte(&self) -> u8 {
        match self {
            Flag::ColoursAvailable |
            Flag::BoldfaceAvailable |
            Flag::ItalicAvailable |
            Flag::FixedSpaceAvailable |
            Flag::TimedKeyboardInputAvailable => 0x01,
            Flag::TranscriptRunning |
            Flag::ForceFixedPitchFont |
            Flag::UsePictures |
            Flag::UseUndoOpcodes |
            Flag::UseMouse |
            Flag::UseColours |
            Flag::UseSoundEffects => 0x10
        }
    }

    pub fn mask(&self) -> u8 {
        match self {
            Flag::ColoursAvailable => 0x1,
            Flag::BoldfaceAvailable => 0x4,
            Flag::ItalicAvailable => 0x8,
            Flag::FixedSpaceAvailable => 0x10,
            Flag::TimedKeyboardInputAvailable => 0x80,
            Flag::TranscriptRunning => 0x1,
            Flag::ForceFixedPitchFont => 0x2,
            Flag::UsePictures => 0x8,
            Flag::UseUndoOpcodes => 0x10,
            Flag::UseMouse => 0x20,
            Flag::UseColours => 0x40,
            Flag::UseSoundEffects => 0x80
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct StaticHeader {
    pub version: u8,
    pub high_memory: ByteAddress,
    pub initial_program_counter: ByteAddress,
    pub dictionary: ByteAddress,
    pub object_table: ByteAddress,
    pub global_variables: ByteAddress,
    pub static_memory: ByteAddress,
    pub abbreviations_table: ByteAddress,
    pub file_length: u16,
    pub checksum: u16
}
impl StaticHeader {
    pub fn file_length(&self) -> usize {
        self.file_length as usize * 4
    }
}

pub fn parse_header(memory: &Memory) -> StaticHeader {
    let version = memory.at(AbsoluteAddress(VERSION));
    let high_memory = ByteAddress(memory.u16_at(AbsoluteAddress(HIGH_MEMORY)));
    let initial_program_counter = ByteAddress(memory.u16_at(AbsoluteAddress(INITIAL_PROGRAM_COUNTER)));
    let dictionary = ByteAddress(memory.u16_at(AbsoluteAddress(DICTIONARY)));
    let object_table = ByteAddress(memory.u16_at(AbsoluteAddress(OBJECT_TABLE)));
    let global_variables = ByteAddress(memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
    let static_memory = ByteAddress(memory.u16_at(AbsoluteAddress(STATIC_MEMORY)));
    let abbreviations_table = ByteAddress(memory.u16_at(AbsoluteAddress(ABBREVIATIONS)));
    let file_length = memory.u16_at(AbsoluteAddress(FILE_LENGTH));
    let checksum = memory.u16_at(AbsoluteAddress(CHECKSUM));

    StaticHeader {
        version,
        high_memory,
        initial_program_counter,
        dictionary,
        object_table,
        global_variables,
        static_memory,
        abbreviations_table,
        file_length,
        checksum
    }
}

pub fn initialise_header(memory: &mut Memory) {
    memory.set(AbsoluteAddress(FLAGS_1), 0x00);
    memory.set(AbsoluteAddress(FLAGS_2), 0x00);
    memory.set(AbsoluteAddress(INTERPRETER_NUMBER), 0x00);
    memory.set(AbsoluteAddress(INTERPRETER_VERSION), 0x00);
    memory.set(AbsoluteAddress(LINES), 0x19);
    memory.set(AbsoluteAddress(COLUMNS), 0x50);
    memory.set_u16(AbsoluteAddress(WIDTH), 0x19);
    memory.set_u16(AbsoluteAddress(HEIGHT), 0x50);
    memory.set(AbsoluteAddress(FONT_WIDTH), 0x19);
    memory.set(AbsoluteAddress(FONT_HEIGHT), 0x50);
    memory.set(AbsoluteAddress(BACKGROUND), 0x00);
    memory.set(AbsoluteAddress(FOREGROUND), 0x00);
    memory.set_u16(AbsoluteAddress(REVISION_NUMBER), 0x00);
}

pub fn set_flag(memory: &mut Memory, flag: &Flag) {
    let address = AbsoluteAddress(flag.byte() as usize);
    let current_value = memory.at(address);
    let new_value = current_value | flag.mask();
    memory.set(address, new_value);
}

pub fn clear_flag(memory: &mut Memory, flag: Flag) {
    let address = AbsoluteAddress(flag.byte() as usize);
    let current_value = memory.at(address);
    let new_value = current_value & !flag.mask();
    memory.set(address, new_value);
}

pub fn get_flag(memory: &Memory, flag: Flag) -> bool {
    let address = AbsoluteAddress(flag.byte() as usize);
    return memory.at(address) | flag.mask() != 0
}
