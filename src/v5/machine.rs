use std::cmp::min;
use std::io::{BufRead, BufReader};
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Result;
use std::io::Write;
use std::mem::transmute;

use rand::ChaChaRng;
use rand::FromEntropy;
use rand::Rng;
use rand::SeedableRng;

use common::address::AbsoluteAddress;
use common::address::ByteAddress;
use common::address::ToAbsoluteAddress;
use common::branch::BranchArgument;
use common::constant::header::GLOBAL_VARIABLES;
use common::input::InputStream;
use common::instruction::Form;
use common::memory::Memory;
use common::operand::Operand;
use common::output::OutputStream;
use common::stack::Stack;
use v5::address::PackedAddress;
use v5::header::initialise_header;
use v5::header::parse_header;
use v5::instruction::decode_instruction;
use v5::instruction::Instruction;
use v5::memory::calculate_checksum;
use v5::memory::describe_memory;
use v5::memory::validate_with_checksum;
use v5::objects::AccessObject;
use v5::objects::AccessObjects;
use v5::objects::AccessProperties;
use v5::objects::AccessProperty;
use v5::objects::get_prop_len;
use v5::objects::ModifyObject;
use v5::objects::ModifyObjects;
use v5::objects::ModifyProperties;
use v5::objects::ModifyProperty;
use v5::objects::MutableObjectTableView;
use v5::objects::ObjectId;
use v5::objects::ObjectTableView;
use v5::output::Output;
use v5::routine::decode_routine;
use v5::tokens::Tokeniser;
use v5::z_char::DEFAULT_ALPHABET_V2;
use v5::z_char::V5Decoder;
use v5::z_char::V5Encoder;

fn check_operand_count_eq(instruction: &Instruction, expected_operands: usize) -> Result<()> {
    let actual_operands = instruction.operands.len();
    if actual_operands == expected_operands {
        Ok(())
    }
    else {
        Err(Error::new(
            ErrorKind::Other,
            format!("Expected {} operands, there were {}", expected_operands, actual_operands)))
    }
}

fn check_u16_value_neq(not: u16, value: u16) -> Result<()> {
    if not != value {
        Ok(())
    }
    else {
        Err(Error::new(
            ErrorKind::Other,
            format!("Illegal value received, {}", value)))
    }
}

fn check_operand_count_between(instruction: &Instruction, lower_bound: usize, upper_bound: usize) -> Result<()> {
    if lower_bound >= upper_bound {
        return Err(Error::new(
            ErrorKind::Other,
            format!("Lower bound {} must be less than upper bound {}", lower_bound, upper_bound)));
    }

    let actual_operands = instruction.operands.len();
    if actual_operands < lower_bound {
        Err(Error::new(
            ErrorKind::Other,
            format!("Actual {} operands, fewer than lower bound {}", actual_operands, lower_bound)))
    } else if actual_operands > upper_bound {
        Err(Error::new(
            ErrorKind::Other,
            format!("Actual {} operands, greater than upper bound {}", actual_operands, upper_bound)))
    } else {
        Ok(())
    }
}

fn print_interactive_mode_usage() -> Result<()> {
    writeln!(OutputStream::Stderr, "At the interactive prompt you can provide instructions on how to proceed:")?;
    writeln!(OutputStream::Stderr, " next, n - execute the next instruction")?;
    writeln!(OutputStream::Stderr, " continue, c - resume execution to the next breakpoint")?;
    writeln!(OutputStream::Stderr, " help, h - display the help available for the interactive mode")?;
    writeln!(OutputStream::Stderr, " stack, s - print the stack of calling routines")?;
    writeln!(OutputStream::Stderr, " objects, o - print the objects")?;
    writeln!(OutputStream::Stderr, " quit, q - end the execution of the program")?;
    writeln!(OutputStream::Stderr, " ?x - query the variable x")?;
    writeln!(OutputStream::Stderr)?;
    Ok(())
}

fn print_with_address(stream: &mut OutputStream, stack: &Stack, message: String) -> Result<()> {
    write!(stream, "{:08X} =>", stack.top().program_counter)?;
    for _ in stack.stack.iter() {
        write!(stream, "  ")?;
    }
    stream.write_all(message.as_bytes())?;
    writeln!(stream)
}

pub struct Machine {
    stack: Stack,
    memory: Memory,
    rng: ChaChaRng,
    string_decoder: V5Decoder,
    string_encoder: V5Encoder,
    output: Output,
    input: BufReader<InputStream>,
    is_debug: bool,
    breakpoints: Vec<AbsoluteAddress>
}
impl Machine {
    pub fn initialise(mut memory: Memory, verbose: OutputStream, transcript: OutputStream, input: BufReader<InputStream>, is_debug: bool, breakpoints: Vec<AbsoluteAddress>) -> Result<Machine> {
        let header = parse_header(&memory);

        validate_with_checksum(&memory, &header)?;

        initialise_header(&mut memory);

        if is_debug || !breakpoints.is_empty() {
            print_interactive_mode_usage()?;
        }

        Ok(Machine {
            stack: Stack::new(header.initial_program_counter.to_address()),
            memory,
            rng: ChaChaRng::from_entropy(),
            string_decoder: V5Decoder::new(DEFAULT_ALPHABET_V2),
            string_encoder: V5Encoder::new(DEFAULT_ALPHABET_V2),
            output: Output::new().with_debug(verbose).with_transcript(transcript),
            input,
            is_debug,
            breakpoints
        })
    }

    pub fn print_state_to_error(&mut self) -> Result<()> {
        self.stack.print_stack(self.output.error())
    }

    pub fn print_memory(&self) -> Result<()> {
        describe_memory(OutputStream::Stdout, &self.memory)
    }

    fn predictable_rng(&mut self, seed: <ChaChaRng as SeedableRng>::Seed) {
        self.rng = ChaChaRng::from_seed(seed);
    }

    fn unpredictable_rng(&mut self) {
        self.rng = ChaChaRng::from_entropy();
    }

    fn get_operand_value(&mut self, instruction: &Instruction, index: usize) -> Result<u16> {
        let operand = instruction
            .operands
            .get(index)
            .ok_or(Error::new(ErrorKind::Other, format!("Missing operand: {}", index)))?;

        let value = match operand {
            Operand::SmallConstant(v) => *v as u16,
            Operand::LargeConstant(v) => *v,
            Operand::Variable(v) => {
                self.get_variable(*v)
            }
        };

        Ok(value)
    }

    fn get_operand_i16_value(&mut self, instruction: &Instruction, index: usize) -> Result<i16> {
        let unsigned_value = self.get_operand_value(instruction, index)?;
        let value = unsafe { transmute::<u16, i16>(unsigned_value) };
        Ok(value)
    }

    fn get_variable(&mut self, variable: u8) -> u16 {
        let current_frame = self.stack.top_mut();
        if variable == 0 {
            current_frame.routine_stack.pop().unwrap()
        } else if variable < 16 {
            current_frame.local_variables[(variable - 1) as usize]
        } else {
            let variable_offset = (variable - 0x10) as usize * 2;
            let global_variables_start = ByteAddress(self.memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
            let variable_address = global_variables_start.to_address() + variable_offset;
            self.memory.u16_at(variable_address)
        }
    }

    fn set_variable(&mut self, variable: u8, value: u16) {
        let current_frame = self.stack.top_mut();
        if variable == 0 {
            current_frame.routine_stack.push(value);
        } else if variable < 16 {
            current_frame.local_variables[(variable - 1) as usize] = value;
        } else {
            let variable_offset = (variable as usize - 0x10) * 2;
            let global_variables_start = ByteAddress(self.memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
            let variable_address = global_variables_start.to_address() + variable_offset;
            self.memory.set_u16(variable_address, value);
        }
    }

    fn get_i16_variable(&mut self, variable: u8) -> i16 {
        let current_frame = self.stack.top_mut();
        let unsigned_value = if variable == 0 {
            current_frame.routine_stack.pop().unwrap()
        } else if variable < 16 {
            current_frame.local_variables[(variable - 1) as usize]
        } else {
            let variable_offset = (variable - 0x10) as usize * 2;
            let global_variables_start = ByteAddress(self.memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
            let variable_address = global_variables_start.to_address() + variable_offset;
            self.memory.u16_at(variable_address)
        };
        unsafe { transmute::<u16, i16>(unsigned_value) }
    }

    fn set_i16_variable(&mut self, variable: u8, value: i16) {
        let unsigned_value = unsafe { transmute::<i16, u16>(value) };
        let current_frame = self.stack.top_mut();
        if variable == 0 {
            current_frame.routine_stack.push(unsigned_value);
        } else if variable < 16 {
            current_frame.local_variables[(variable - 1) as usize] = unsigned_value;
        } else {
            let variable_offset = (variable as usize - 0x10) * 2;
            let global_variables_start = ByteAddress(self.memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
            let variable_address = global_variables_start.to_address() + variable_offset;
            self.memory.set_u16(variable_address, unsigned_value);
        }
    }

    fn store_operation_result(&mut self, instruction: &Instruction, value: u16) {
        let result_argument = instruction.store.unwrap();
        self.set_variable(result_argument, value);
        self.stack.top_mut().program_counter += instruction.size;
    }

    fn store_operation_i16_result(&mut self, instruction: &Instruction, value: i16) {
        let result_argument = instruction.store.unwrap();
        self.set_i16_variable(result_argument, value);
        self.stack.top_mut().program_counter += instruction.size;
    }

    fn return_from_routine(&mut self, value: u16) -> Result<()> {
        let frame = self.stack.pop();

        write!(self.output.debug(), "{:08X} =>", frame.program_counter)?;
        for _ in self.stack.stack.iter() {
            write!(self.output.debug(), "  ")?;
        }
        writeln!(self.output.debug(), "  returning {} to {:08X}", value, self.stack.top().program_counter)?;

        match frame.store_result {
            Some(result_argument) => {
                self.set_variable(result_argument, value);
            },
            _ => {}
        }
        Ok(())
    }

    fn on_conditional_success(&mut self, instruction: Instruction) -> Result<()> {
        match instruction.branch.unwrap() {
            BranchArgument::ReturnTrue { reverse_logic, resume_address } => {
                if reverse_logic {
                    self.stack.top_mut().program_counter = resume_address;
                } else {
                    self.return_from_routine(1)?;
                }
            },
            BranchArgument::ReturnFalse { reverse_logic, resume_address } => {
                if reverse_logic {
                    self.stack.top_mut().program_counter = resume_address;
                } else {
                    self.return_from_routine(0)?;
                }
            },
            BranchArgument::Jump { reverse_logic, target_address, resume_address } => {
                if reverse_logic {
                    self.stack.top_mut().program_counter = resume_address;
                } else {
                    self.stack.top_mut().program_counter = target_address;
                }
            }
        }

        Ok(())
    }

    fn on_conditional_failure(&mut self, instruction: Instruction) -> Result<()> {
        match instruction.branch.unwrap() {
            BranchArgument::ReturnTrue { reverse_logic, resume_address } => {
                if reverse_logic {
                    self.return_from_routine(1)?;
                } else {
                    self.stack.top_mut().program_counter = resume_address;
                }
            },
            BranchArgument::ReturnFalse { reverse_logic, resume_address } => {
                if reverse_logic {
                    self.return_from_routine(0)?;
                } else {
                    self.stack.top_mut().program_counter = resume_address;
                }
            },
            BranchArgument::Jump { reverse_logic, target_address, resume_address } => {
                if reverse_logic {
                    self.stack.top_mut().program_counter = target_address;
                } else {
                    self.stack.top_mut().program_counter = resume_address;
                }
            }
        }

        Ok(())
    }

    fn get_object_table(&self) -> ObjectTableView {
        let header = parse_header(&self.memory);
        ObjectTableView {
            memory: &self.memory,
            start_of_object_table: header.object_table.to_address()
        }
    }

    fn get_object_table_mut(&mut self) -> MutableObjectTableView {
        let header = parse_header(&self.memory);
        MutableObjectTableView {
            memory: &mut self.memory,
            start_of_object_table: header.object_table.to_address()
        }
    }

    fn flush(&mut self) -> Result<()> {
        self.output.streams().flush()?;
        self.output.debug().flush()?;
        self.output.error().flush()?;
        Ok(())
    }

    fn internal_step(&mut self) -> Result<bool> {
        let pc = self.stack.top().program_counter;
        match decode_instruction(&self.memory, &mut self.string_decoder, pc) {
            Ok(r) => {
                print_with_address(self.output.debug(), &self.stack, format!("instruction {}", r))?;
                match (r.opcode, r.form) {
                    (0, Form::Extended) => self.save(r),
                    (1, Form::Extended) => self.restore(r),
                    (9, Form::Extended) => self.save_undo(r),
                    (10, Form::Extended) => self.restore_undo(r),
                    (_, Form::Extended) => Err(Error::new(ErrorKind::Other, format!("{:08X} => unable to handle extended form instruction: {:?}", pc, r))),
                    (1, _) => self.je(r),
                    (2, _) => self.jl(r),
                    (3, _) => self.jg(r),
                    (6, _) => self.jin(r),
                    (8, _) => self.or(r),
                    (9, _) => self.and(r),
                    (10, _) => self.test_attr(r),
                    (11, _) => self.set_attr(r),
                    (12, _) => self.clear_attr(r),
                    (13, _) => self.store(r),
                    (14, _) => self.insert_obj(r),
                    (15, _) => self.loadw(r),
                    (16, _) => self.loadb(r),
                    (17, _) => self.get_prop(r),
                    (18, _) => self.get_prop_addr(r),
                    (20, _) => self.add(r),
                    (21, _) => self.sub(r),
                    (22, _) => self.mul(r),
                    (23, _) => self.div(r),
                    (24, _) => self.mod_remainder(r),
                    (25, _) => self.call(r),
                    (26, _) => self.call(r),
                    (128, _) => self.jz(r),
                    (129, _) => self.get_sibling(r),
                    (130, _) => self.get_child(r),
                    (133, _) => self.inc(r),
                    (134, _) => self.dec(r),
                    (135, _) => self.print_addr(r),
                    (136, _) => self.call(r),
                    (137, _) => self.remove_obj(r),
                    (138, _) => self.print_obj(r),
                    (139, _) => self.ret(r),
                    (140, _) => self.jump(r),
                    (141, _) => self.print_paddr(r),
                    (143, _) => self.call(r),
                    (144, _) => self.jz(r),
                    (145, _) => self.get_sibling(r),
                    (146, _) => self.get_child(r),
                    (149, _) => self.inc(r),
                    (150, _) => self.dec(r),
                    (155, _) => self.ret(r),
                    (160, _) => self.jz(r),
                    (161, _) => self.get_sibling(r),
                    (162, _) => self.get_child(r),
                    (163, _) => self.get_parent(r),
                    (164, _) => self.get_prop_len(r),
                    (167, _) => self.print_addr(r),
                    (168, _) => self.call(r),
                    (169, _) => self.remove_obj(r),
                    (170, _) => self.print_obj(r),
                    (171, _) => self.ret(r),
                    (173, _) => self.print_paddr(r),
                    (175, _) => self.call(r),
                    (176, _) => self.rtrue(r),
                    (177, _) => self.rfalse(r),
                    (178, _) => self.print(r),
                    (179, _) => self.print_ret(r),
                    (184, _) => self.ret_popped(r),
                    (186, _) => self.quit(r),
                    (187, _) => self.new_line(r),
                    (189, _) => self.verify(r),
                    (224, _) => self.call(r),
                    (225, _) => self.storew(r),
                    (226, _) => self.storeb(r),
                    (227, _) => self.put_prop(r),
                    (228, _) => self.read(r),
                    (229, _) => self.print_char(r),
                    (230, _) => self.print_num(r),
                    (231, _) => self.random(r),
                    (232, _) => self.push(r),
                    (233, _) => self.pull(r),
                    (234, _) => self.split_window(r),
                    (235, _) => self.set_window(r),
                    (236, _) => self.call(r),
                    (239, _) => self.set_cursor(r),
                    (241, _) => self.set_text_style(r),
                    (246, _) => self.read_char(r),
                    (248, _) => self.not(r),
                    (249, _) => self.call(r),
                    (250, _) => self.call(r),
                    (251, _) => self.tokenise(r),
                    (252, _) => self.encode_text(r),
                    (255, _) => self.check_arg_count(r),
                    _code => {
                        Err(Error::new(ErrorKind::Other, format!("{:08X} => unable to handle instruction: {:?}", pc, r)))
                    }
                }
            },
            Err(err) => {
                writeln!(self.output.error(), "{:08X} => failed to decode instruction: {:?}", pc, err)?;
                Err(err)
            }
        }
    }

    pub fn step(&mut self) -> Result<bool> {
        let pc = self.stack.top().program_counter;
        if self.breakpoints.contains(&pc) {
            self.is_debug = true;
        }

        if self.is_debug {
            loop {
                write!(self.output.error(), "!> ")?;
                let mut line = String::new();
                self.input.read_line(&mut line)?;
                if line.eq_ignore_ascii_case("c\n") || line.eq_ignore_ascii_case("continue\n") {
                    self.is_debug = false;
                    break;
                } else if line.eq_ignore_ascii_case("n\n") || line.eq_ignore_ascii_case("next\n") {
                    break;
                } else if line.eq_ignore_ascii_case("h\n") || line.eq_ignore_ascii_case("help\n") {
                    print_interactive_mode_usage()?;
                } else if line.eq_ignore_ascii_case("s\n") || line.eq_ignore_ascii_case("stack\n") {
                    self.stack.print_stack(self.output.error())?;
                } else if line.eq_ignore_ascii_case("o\n") || line.eq_ignore_ascii_case("objects\n") {
                    unsafe {
                        let out = self.output.error() as *mut OutputStream;
                        // The object table does not need access to the output
                        let object_table = self.get_object_table();
                        writeln!(*out, "{:?}", object_table)?;
                    };
                } else if line.eq_ignore_ascii_case("q\n") || line.eq_ignore_ascii_case("quit\n") {
                    return Ok(false);
                } else if line.starts_with("?") {
                    let query = line.split_at(1).1.trim();
                    match query.parse::<u8>() {
                        Ok(variable) => {
                            let current_frame = self.stack.top_mut();
                            if variable == 0 {
                                match current_frame.routine_stack.last() {
                                    Some(value) => writeln!(self.output.error(), "{}", value)?,
                                    None => writeln!(self.output.error(), "The stack is empty")?
                                }
                            } else if variable < 16 {
                                if ((variable - 1) as usize) < current_frame.local_variables.len() {
                                    writeln!(self.output.error(), "{}", current_frame.local_variables[(variable - 1) as usize])?;
                                } else {
                                    writeln!(self.output.error(), "No local variable {}", variable)?;
                                }
                            } else {
                                let variable_offset = (variable - 0x10) as usize * 2;
                                let global_variables_start = ByteAddress(self.memory.u16_at(AbsoluteAddress(GLOBAL_VARIABLES)));
                                let variable_address = global_variables_start.to_address() + variable_offset;
                                writeln!(self.output.error(), "{}", self.memory.u16_at(variable_address))?;
                            }
                        },
                        Err(_e) => {
                            writeln!(self.output.error(), "Failed to parse '{}' as variable", query)?;
                        }
                    }
                } else {
                    writeln!(self.output.error(), "Do not understand '{}', try help", line.trim())?;
                }
            }
        }

        let r = self.internal_step();
        self.flush()?;
        match &r {
            Ok(result) => Ok(*result),
            Err(err) => {
                writeln!(self.output.error())?;
                writeln!(self.output.error(), "Error:")?;
                writeln!(self.output.error(), "{}", err)?;
                writeln!(self.output.error())?;
                self.print_state_to_error()?;
                self.flush()?;
                Ok(false)
            }
        }
    }

    fn add(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let result = self.get_operand_i16_value(&r, 0)? + self.get_operand_i16_value(&r, 1)?;

        self.store_operation_i16_result(&r, result);
        Ok(true)
    }

    fn sub(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let result = self.get_operand_i16_value(&r, 0)? - self.get_operand_i16_value(&r, 1)?;

        self.store_operation_i16_result(&r, result);
        Ok(true)
    }

    fn mul(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let result = self.get_operand_i16_value(&r, 0)? * self.get_operand_i16_value(&r, 1)?;

        self.store_operation_i16_result(&r, result);
        Ok(true)
    }

    fn div(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let result = self.get_operand_i16_value(&r, 0)? / self.get_operand_i16_value(&r, 1)?;

        self.store_operation_i16_result(&r, result);
        Ok(true)
    }

    fn inc(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let variable = self.get_operand_i16_value(&r, 0)? as u8;

        let current_value = self.get_i16_variable(variable);
        self.set_i16_variable(variable, current_value + 1);

        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn dec(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let variable = self.get_operand_value(&r, 0)? as u8;

        let current_value = self.get_i16_variable(variable);
        self.set_i16_variable(variable, current_value - 1);

        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn mod_remainder(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let result = self.get_operand_i16_value(&r, 0)? % self.get_operand_i16_value(&r, 1)?;

        self.store_operation_i16_result(&r, result);
        Ok(true)
    }

    fn or(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let result = self.get_operand_value(&instruction, 0)? | self.get_operand_value(&instruction, 1)?;

        self.store_operation_result(&instruction, result);
        Ok(true)
    }

    fn and(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let result = self.get_operand_value(&instruction, 0)? & self.get_operand_value(&instruction, 1)?;

        self.store_operation_result(&instruction, result);
        Ok(true)
    }

    fn not(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let result = !self.get_operand_value(&instruction, 0)?;

        self.store_operation_result(&instruction, result);
        Ok(true)
    }

    fn push(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let value = self.get_operand_value(&instruction, 0)?;
        self.set_variable(0, value);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn pull(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let value = self.get_variable(0);
        let variable = self.get_operand_value(&instruction, 0)?;
        self.set_variable(variable as u8, value);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn store(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let variable = self.get_operand_value(&r, 0)? as u8;
        let value = self.get_operand_value(&r, 1)?;

        self.set_variable(variable, value);

        self.stack.top_mut().program_counter += r.size;

        Ok(true)
    }

    fn storew(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 3)?;
        let array = self.get_operand_value(&instruction, 0)?;
        let word_index = self.get_operand_value(&instruction, 1)?;
        let value = self.get_operand_value(&instruction, 2)?;

        self.memory.set_u16(ByteAddress(array + (2 * word_index)).to_address(), value);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn storeb(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 3)?;
        let array = self.get_operand_value(&instruction, 0)?;
        let index = self.get_operand_value(&instruction, 1)?;
        let value = self.get_operand_value(&instruction, 2)? as u8;

        self.memory.set(ByteAddress(array + index).to_address(), value);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn loadw(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let array = self.get_operand_value(&instruction, 0)?;
        let word_index = self.get_operand_value(&instruction, 1)?;
        let result = self.memory.u16_at(ByteAddress(array + (2 * word_index)).to_address());

        self.store_operation_result(&instruction, result);
        Ok(true)
    }

    fn loadb(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let array = self.get_operand_value(&instruction, 0)?;
        let word_index = self.get_operand_value(&instruction, 1)?;
        let result = self.memory.at(ByteAddress(array + word_index).to_address());

        self.store_operation_result(&instruction, result as u16);
        Ok(true)
    }

    fn random(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let range = self.get_operand_value(&r, 0)?;
        let result = self.rng.gen_range(1, range + 1);

        self.store_operation_result(&r, result);
        Ok(true)
    }

    fn call(&mut self, instruction: Instruction) -> Result<bool> {
        let routine_addr = PackedAddress(self.get_operand_value(&instruction, 0)?);
        if routine_addr == PackedAddress(0) {
            print_with_address(self.output.debug(), &self.stack, format!("not calling {:08X} ",routine_addr))?;
            match instruction.store {
                Some(result_argument) => {
                    self.stack.top_mut().program_counter += instruction.size;
                    self.set_variable(result_argument, 0);
                },
                None => {
                    self.stack.top_mut().program_counter += instruction.size;
                }
            }
            return Ok(true);
        }

        let routine = decode_routine(&self.memory, routine_addr)?;

        // Initialise the routine local variables with 0-values
        let mut variables = vec![0; routine.number_of_local_variables as usize];
        // Set the local variables to the values of the arguments passed in
        let argument_count = instruction.operands.len() - 1;
        let variables_to_initialise = min(argument_count, routine.number_of_local_variables as usize);
        for argument_index in 0..variables_to_initialise {
            variables[argument_index] = self.get_operand_value(&instruction, argument_index + 1)?;
        }

        match instruction.store {
            Some(result_argument) => {
                print_with_address(self.output.debug(), &self.stack, format!("calling {:08X} ",routine_addr))?;
                self.stack.top_mut().program_counter += instruction.size;
                self.stack.call_routine_and_store_result(routine.body, argument_count as u8, variables, result_argument);
            },
            None => {
                print_with_address(self.output.debug(), &self.stack, format!("calling {:08X} ",routine_addr))?;
                self.stack.top_mut().program_counter += instruction.size;
                self.stack.call_routine_and_discard_result(routine.body, argument_count as u8, variables);
            }
        }
        Ok(true)
    }

    fn jump(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;

        let offset = self.get_operand_i16_value(&r, 0)?;
        self.stack.top_mut().program_counter += r.size;
        self.stack.top_mut().program_counter += offset - 2;
        Ok(true)
    }

    fn je(&mut self, instruction: Instruction) -> Result<bool> {
        let matched: bool = {
            let left = self.get_operand_i16_value(&instruction, 0)?;
            let mut index = 1;
            loop {
                if index < instruction.operands.len() {
                    let right = self.get_operand_i16_value(&instruction, index)?;
                    if left == right {
                        break true;
                    }
                    index += 1;
                } else {
                    break false;
                }
            }
        };

        if matched {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        }
        Ok(true)
    }

    fn jl(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let left = self.get_operand_i16_value(&instruction, 0)?;
        let right = self.get_operand_i16_value(&instruction, 1)?;
        if left < right {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        };
        Ok(true)
    }

    fn jg(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let left = self.get_operand_i16_value(&instruction, 0)?;
        let right = self.get_operand_i16_value(&instruction, 1)?;
        if left > right {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        };
        Ok(true)
    }

    fn jz(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let value = self.get_operand_i16_value(&instruction, 0)?;
        if value == 0 {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        };
        Ok(true)
    }

    fn check_arg_count(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let value = self.get_operand_value(&instruction, 0)?;
        if value <= self.stack.top().arg_count as u16 {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        };
        Ok(true)
    }

    fn print(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 0)?;
        write!(self.output.streams(), "{}", r.text.unwrap())?;
        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn print_ret(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 0)?;
        writeln!(self.output.streams(), "{}", r.text.unwrap())?;
        self.return_from_routine(1)?;
        Ok(true)
    }

    fn print_num(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let num = self.get_operand_value(&r, 0)?;
        write!(self.output.streams(), "{}", num)?;
        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn new_line(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 0)?;
        writeln!(self.output.streams())?;
        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn print_addr(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let address = ByteAddress(self.get_operand_value(&r, 0)?);
        let (string, _) = self.string_decoder.decode_string(&self.memory, address.to_address())?;
        write!(self.output.streams(), "{}", string)?;
        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn print_paddr(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let address = PackedAddress(self.get_operand_value(&r, 0)?);
        let (string, _) = self.string_decoder.decode_string(&self.memory, address.to_address())?;
        write!(self.output.streams(), "{}", string)?;
        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn print_char(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let c = self.get_operand_value(&instruction, 0)?;
        if c != 0 {
            write!(self.output.streams(), "{}", self.string_decoder.decode_zscii(c)?)?;
        }
        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn rfalse(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.return_from_routine(0)?;
        Ok(true)
    }

    fn rtrue(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.return_from_routine(1)?;
        Ok(true)
    }

    fn ret(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let value = self.get_operand_value(&r, 0)?;
        self.return_from_routine(value)?;
        Ok(true)
    }

    fn ret_popped(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        let value = self.get_variable(0);
        self.return_from_routine(value)?;
        Ok(true)
    }

    fn quit(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        Ok(false)
    }

    fn get_prop(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let object_id = self.get_operand_value(&r, 0)?;
        let property_number = self.get_operand_value(&r, 1)? as u8;

        let property_value = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_properties_table()
            .find_property(property_number)
            .map(|property| property.get_u16());

        let value = property_value.unwrap_or_else(|| {
            self.get_object_table()
                .get_default_property_value(property_number)
        });

        self.store_operation_result(&r, value);
        Ok(true)
    }

    fn put_prop(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 3)?;
        let object_id = self.get_operand_value(&r, 0)?;
        let property_number = self.get_operand_value(&r, 1)? as u8;
        let property_value = self.get_operand_value(&r, 2)? as u16;

        let mut object_table = self.get_object_table_mut();
        let mut object = object_table.get_object_mut(ObjectId(object_id));
        let mut properties = object.get_properties_table_mut();
        let prop = properties.find_property_mut(property_number);

        match prop {
            Some(mut property) => {
                Ok(property.set_u16(property_value))
            },
            None => Err(Error::new(ErrorKind::Other, "No property found"))
        }?;

        self.stack.top_mut().program_counter += r.size;
        Ok(true)
    }

    fn get_prop_addr(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 2)?;
        let object_id = self.get_operand_value(&r, 0)?;
        let property = self.get_operand_value(&r, 1)? as u8;

        let address = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_properties_table()
            .find_property_addr(property)
            .unwrap_or(AbsoluteAddress(0));

        self.store_operation_result(&r, address.0 as u16);
        Ok(true)
    }

    fn get_prop_len(&mut self, r: Instruction) -> Result<bool> {
        check_operand_count_eq(&r, 1)?;
        let address = self.get_operand_value(&r, 0)? as usize;

        let length = get_prop_len(&self.memory, AbsoluteAddress(address));

        self.store_operation_result(&r, length as u16);
        Ok(true)
    }

    fn jin(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let object_id_0 = self.get_operand_value(&instruction, 0)?;
        check_u16_value_neq(0, object_id_0)?;
        let object_id_1 = self.get_operand_value(&instruction, 1)?;

        let is_equal = self
            .get_object_table()
            .get_object(ObjectId(object_id_0))
            .get_parent()
            .eq(&ObjectId(object_id_1));

        if is_equal {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        };
        Ok(true)
    }

    fn get_child(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        check_u16_value_neq(0, object_id)?;

        let child = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_child();

        let result_argument = instruction.store.unwrap();
        self.set_variable(result_argument, child.0);

        if child != ObjectId(0) {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        }
        Ok(true)
    }

    fn get_sibling(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        check_u16_value_neq(0, object_id)?;

        let sibling = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_sibling();

        let result_argument = instruction.store.unwrap();
        self.set_variable(result_argument, sibling.0);

        if sibling != ObjectId(0) {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        }
        Ok(true)
    }

    fn get_parent(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        check_u16_value_neq(0, object_id)?;

        let parent = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_parent();

        self.store_operation_result(&instruction, parent.0);
        Ok(true)
    }

    fn print_obj(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        check_u16_value_neq(0, object_id)?;

        let name_address = self
            .get_object_table()
            .get_object(ObjectId(object_id))
            .get_properties_table()
            .get_name_address();
        let object_name = self.string_decoder.decode_string(&self.memory, name_address)?;

        write!(self.output.streams(), "{}", object_name.0)?;
        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn insert_obj(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        let destination_id = self.get_operand_value(&instruction, 1)?;

        self
            .get_object_table_mut()
            .insert_obj(ObjectId(object_id), ObjectId(destination_id));

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn remove_obj(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;
        let object_id = self.get_operand_value(&instruction, 0)?;

        self
            .get_object_table_mut()
            .remove_obj(ObjectId(object_id));

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn test_attr(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        let attribute = self.get_operand_value(&instruction, 1)? as u8;

        let has_attribute = {
            let object_table = self.get_object_table();

            let object_view = object_table.get_object(ObjectId(object_id));
            object_view.get_attribute(attribute)
        };

        if has_attribute {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        }
        Ok(true)
    }

    fn set_attr(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        let attribute = self.get_operand_value(&instruction, 1)? as u8;

        let mut object_table = self.get_object_table_mut();

        let mut object_view = object_table.get_object_mut(ObjectId(object_id));
        object_view.set_attribute(attribute, true);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn clear_attr(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;
        let object_id = self.get_operand_value(&instruction, 0)?;
        let attribute = self.get_operand_value(&instruction, 1)? as u8;

        let mut object_table = self.get_object_table_mut();

        let mut object_view = object_table.get_object_mut(ObjectId(object_id));
        object_view.set_attribute(attribute, false);

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn encode_text(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 4)?;
        let input_buffer = ByteAddress(self.get_operand_value(&instruction, 0)?).to_address();
        let length = self.get_operand_value(&instruction, 1)?;
        let offset = self.get_operand_value(&instruction, 2)?;
        let output_buffer = ByteAddress(self.get_operand_value(&instruction, 3)?).to_address();

        self.string_encoder.encode_string(&mut self.memory, input_buffer + offset, length, output_buffer)?;

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn read(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;

        let text_buffer = ByteAddress(self.get_operand_value(&instruction, 0)?).to_address();
        let parse_buffer = ByteAddress(self.get_operand_value(&instruction, 1)?);

        let max_characters = self.memory.at(text_buffer) + 1;

        let mut read = 0;
        {
            let mut bytes = self.input.by_ref().bytes();
            for i in 0..max_characters {
                let maybe_next_byte = bytes.next();
                if maybe_next_byte.is_some() {
                    let byte = maybe_next_byte.unwrap()?;
                    if byte == '\n' as u8 {
                        // Handle terminating character
                        break;
                    } else if byte > 0x40 && byte < 0x5B {
                        // Convert upper case to lower case
                        self.memory.set(text_buffer + 2 + i, byte + 0x20);
                        read += 1;
                    } else {
                        self.memory.set(text_buffer + 2 + i, byte);
                        read += 1;
                    }
                } else {
                    break;
                }
            }
        }
        self.memory.set(text_buffer + 1, read);

        if parse_buffer.0 != 0 {
            let tokens = {
                let mut tokeniser = Tokeniser::new_from_header(&self.memory, &mut self.string_encoder)?;
                tokeniser.parse_to_tokens(text_buffer)?
            };

            let mut output_address = parse_buffer.to_address();
            self.memory.set(output_address + 1, tokens.len() as u8);
            output_address += 2;
            for token in tokens {
                let address = token.dictionary_entry;
                self.memory.set_u16(output_address, address.0);
                self.memory.set(output_address + 2, token.length);
                self.memory.set(output_address + 3, token.start);
                output_address += 4;
            }
        }

        self.store_operation_result(&instruction, 13);
        Ok(true)
    }

    fn read_char(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;

        let mut buffer = [0 as u8; 1];
        let read = self.input.read(&mut buffer)?;
        if read != 1 {
            return Err(Error::new(ErrorKind::Other, "Wrong number of bytes read"));
        }

        self.store_operation_result(&instruction, buffer[0].into());
        Ok(true)
    }

    fn tokenise(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_between(&instruction, 2, 3)?;
        let text_buffer = ByteAddress(self.get_operand_value(&instruction, 0)?).to_address();
        let parse_buffer = ByteAddress(self.get_operand_value(&instruction, 1)?).to_address();

        let tokens = if instruction.operands.len() > 2 {
            let dictionary_address = ByteAddress(self.get_operand_value(&instruction, 2)?).to_address();
            let mut tokeniser = Tokeniser::new_for_dictionary(&self.memory, &mut self.string_encoder, dictionary_address)?;
            tokeniser.parse_to_tokens(text_buffer)?
        } else {
            let mut tokeniser = Tokeniser::new_from_header(&self.memory, &mut self.string_encoder)?;
            tokeniser.parse_to_tokens(text_buffer)?
        };

        let mut output_address = parse_buffer.to_address();
        self.memory.set(output_address + 1, tokens.len() as u8);
        output_address += 2;
        for token in tokens {
            let address = token.dictionary_entry;
            self.memory.set_u16(output_address, address.0);
            self.memory.set(output_address + 2, token.length);
            self.memory.set(output_address + 3, token.start);
            output_address += 4;
        }

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn save(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.store_operation_i16_result(&instruction, -1);
        Ok(true)
    }

    fn restore(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.store_operation_i16_result(&instruction, -1);
        Ok(true)
    }

    fn save_undo(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.store_operation_i16_result(&instruction, -1);
        Ok(true)
    }

    fn restore_undo(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        self.store_operation_i16_result(&instruction, -1);
        Ok(true)
    }

    fn verify(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 0)?;
        let header = parse_header(&self.memory);
        let checksum = calculate_checksum(&self.memory, &header);

        if checksum == header.checksum {
            self.on_conditional_success(instruction)?;
        } else {
            self.on_conditional_failure(instruction)?;
        }
        Ok(true)
    }

    fn set_text_style(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;

        writeln!(self.output.debug(), "  Ignoring text style request")?;

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn split_window(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;

        writeln!(self.output.debug(), "  Ignoring window change request")?;

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn set_window(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 1)?;

        writeln!(self.output.debug(), "  Treat change window as new line")?;
        writeln!(self.output.streams())?;

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }

    fn set_cursor(&mut self, instruction: Instruction) -> Result<bool> {
        check_operand_count_eq(&instruction, 2)?;

        writeln!(self.output.debug(), "  Ignoring cursor change request")?;

        self.stack.top_mut().program_counter += instruction.size;
        Ok(true)
    }
}

mod machine_tests {
    use std::io::BufReader;
    use std::iter::repeat;

    use rand::ChaChaRng;
    use rand::FromEntropy;

    use common::address::AbsoluteAddress;
    use common::input::InputStream;
    use common::memory::Memory;
    use common::stack::Stack;
    use v5::output::Output;
    use v5::z_char::DEFAULT_ALPHABET_V2;
    use v5::z_char::V5Decoder;
    use v5::z_char::V5Encoder;

    use super::Machine;

    #[test]
    fn set_and_get_stack_head() {
        let mut m = empty_machine();

        m.set_variable(0, 5);

        assert_eq!(5, m.get_variable(0))
    }

    #[test]
    fn set_and_get_local_variable() {
        let mut m = empty_machine();

        m.set_variable(5, 5);

        assert_eq!(5, m.get_variable(5))
    }

    #[test]
    fn set_and_get_global_variable() {
        let mut m = empty_machine();

        m.set_variable(20, 5);

        assert_eq!(5, m.get_variable(20))
    }

    fn empty_machine() -> Machine {
        let mut stack = Stack::new(AbsoluteAddress(0));
        stack.top_mut().local_variables.extend(repeat(0).take(15));

        Machine {
            stack: stack,
            memory: Memory::new(vec![0; 400]),
            rng: ChaChaRng::from_entropy(),
            string_decoder: V5Decoder::new(DEFAULT_ALPHABET_V2),
            string_encoder: V5Encoder::new(DEFAULT_ALPHABET_V2),
            output: Output::new(),
            input: BufReader::new(InputStream::stdin()),
            is_debug: false,
            breakpoints: Vec::new()
        }
    }
}
